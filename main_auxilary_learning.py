
import os
import time
from time import time

## from torch.utils.tensorboard import SummaryWriter
from torch import optim
from model import Ensamble

from config import get_args
from dataloaders import get_custom_data
from dataloaders import get_train_loader_aux
from loss_objectives import CustomArcface,F1_Loss
from model import build_model
from ranger import Ranger
from utils import save_checkpoint, remove_module_if_needed,save_checkpoint_base
from validation import validate
from train_aux import train,val,train_bald,val_bald
import torch
import numpy as np
from clearml import Task
from minima_prop_utils.utils import get_sharpness,get_nonuniformity
from utils import  load_ensamble_with_weights
import random

best_acc1 = 0

def main():
    global best_acc1
    #torch.backends.cudnn.benchmark = True


    args = get_args()

    args.seed = 909
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    np.random.seed(args.seed)
    random.seed(args.seed)
    args.aux  = True
    temp = args.milestones.split(',')
    args.milestones= [float(f) for f in temp]
    if  not os.path.exists(args.results_dir):
        os.mkdir(args.results_dir)
    args.save_path = args.results_dir +'/'+ args.save_path+"_lr"+str(args.lr)+"_wd"+str(args.weight_decay)+"_"+args.train_data_folder
    if not os.path.exists(args.save_path) and not args.evaluate:
       os.mkdir(args.save_path)
    if not args.evaluate:
        with open(args.save_path+'/args.txt', 'w') as f:
            for k,v in args.__dict__.items():
                f.write(str(k))
                f.write(':')
                f.write(str(v))
                f.write('\n')
    #model =.cuda()
    args.tb_filename = os.path.join(args.save_path,"tb")
    #tb = SummaryWriter(args.tb_filename)

    train_loader,val_loader,num_of_classes = get_train_loader_aux(args)
    #c_val_data=get_custom_data(args.custom_file,args.custom_path)
    #agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df
    args.num_of_classes = num_of_classes
    if args.ensamble:
       model = Ensamble(args).cuda()
       model = load_ensamble_with_weights(model)
    else:
      model = build_model(args)
    if args.dp:
        model = torch.nn.DataParallel(model)
    model = model.cuda()
    weights = torch.tensor([1.,20]).cuda()

    criterion = torch.nn.CrossEntropyLoss(weight=weights)#0.1*F1_Loss()+torch.nn.CrossEntropyLoss(weight=weights)
    #f1_loss = F1_Loss()
    if args.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)

    else:
        #optim.SGD([{'params': [param for name, param in model.named_parameters() if 'fc2' not in name]},
                   #{'params': model.fc2.parameters(), 'lr': 5e-3}], lr=1e-2)
        if args.half:
            optimizer = Ranger(model.parameters(),lr=args.lr, weight_decay=args.weight_decay,eps=1e-4)
        else:
            optimizer = Ranger(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)


    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer,gamma=args.gamma,
                                                     milestones=args.milestones)

    if args.load_path is not "" and not args.ensamble:
        print("Loading: ",args.load_path)
        dict = torch.load(args.load_path)['state_dict']
        model.load_state_dict(dict,strict=False)

        print("Loaded succesfuly")

    if args.bald_cls:

        if args.evaluate:
            val_bald(args, val_loader, model, criterion, optimizer, 0)
        else:
            if args.freeze_bb:
                from utils import freeze_bb
                model = freeze_bb(args,model)
                for epoch in range(1,args.ft_epochs+1):
                    train_bald(args, train_loader, model, criterion, optimizer, epoch,freeze=True)
                    val_bald(args, val_loader, model, criterion, optimizer, epoch)
                    save_checkpoint_base({
                        'epoch': epoch + 1,
                        'arch': args.arch,
                        'state_dict': model.state_dict(),
                        'best_acc1': best_acc1,
                        'optimizer': optimizer.state_dict(),
                    },"bald_recognition_seed"+str(args.seed)+".pth")#+"_f1.pth")


                model = freeze_bb(args, model,unfreeze=True)

            for epoch in range(args.start_epoch,args.epochs+1):
                train_bald(args, train_loader, model, criterion, optimizer, epoch)
                val_bald(args, val_loader, model, criterion, optimizer, epoch)
                scheduler.step()

                save_checkpoint_base({
                        'epoch': epoch + 1,
                           'arch': args.arch,
                        'state_dict': model.state_dict(),
                        'best_acc1': best_acc1,
                        'optimizer' : optimizer.state_dict(),
                    }, "bald_recognition_seed"+str(args.seed)+".pth")#,lfw_acc,ytf_acc,custom_acc,mask_f1,train_top1,acc1)
            # else:
            #     for epoch in range(args.start_epoch,args.epochs+1):
            #
            #         start = time()
            #         train(args, train_loader, model, criterion, optimizer, epoch)
            #         val(args, val_loader, model, criterion, optimizer, epoch)
            #         scheduler.step()
            #
            #     save_checkpoint_base({
            #             'epoch': epoch + 1,
            #                'arch': args.arch,
            #             'state_dict': model.state_dict(),
            #             'best_acc1': best_acc1,
            #             'optimizer' : optimizer.state_dict(),
            #         }, "path.pth")#,lfw_acc,ytf_acc,custom_acc,mask_f1,train_top1,acc1)


if __name__ == '__main__':
    main()