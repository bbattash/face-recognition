import torch
from utils import AverageMeter,ProgressMeter,accuracy
import tqdm
from torch import nn
from time import time
#from kornia.losses import focal_loss
from evaluation_modules import confusion
from inplace_abn import InPlaceABN
from torch.nn import functional as F
torch.autograd.set_detect_anomaly(True)
def cast_running_stats(m):
    if isinstance(m, InPlaceABN):
        m.running_mean = m.running_mean.float()
        m.running_var = m.running_var.float()

def train(args,train_loader,model,criterion, optimizer,epoch,freeze=False):
    model.train()
    if args.q:
        model.qconfig = torch.quantization.get_default_qat_qconfig('fbgemm')
        model_fp32_fused = torch.quantization.fuse_modules(model,[['conv', 'bn', 'relu']])
        model = torch.quantization.prepare_qat(model_fp32_fused)
    elif args.half:
        print("Log:::::::::::::::Half model!")
        model.half()
        model.apply(cast_running_stats)

        for layer in model.modules():
            if isinstance(layer,nn.BatchNorm2d):
                layer.float()
    if args.mask:
        train_top1,t_loss,mask_top1 = train_mask(args, train_loader, model, criterion, optimizer, epoch, freeze)
    else:
        train_top1,t_loss = train_nomask(args, train_loader, model, criterion, optimizer, epoch, freeze)
    return train_top1,t_loss
def train_nomask(args,train_loader,model,criterion, optimizer,epoch,freeze=False):

    if freeze:
        if args.dp:
            model.module.input_layer.eval()
        else:
            model.input_layer.eval()

    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1   = AverageMeter('Acc@1', ':6.2f')
    top5   = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time,losses, top1, top5],
        prefix="Epoch: [{}]".format(epoch))

    begin = time()
    torch.set_printoptions(edgeitems=10)

    for i, (images, target) in enumerate(train_loader):

        # from matplotlib import pyplot as plt
        # fig = plt.figure()
        #
        # transformed_sample = images[0]
        #
        # ax = plt.subplot(1, 3, i + 1)
        # plt.tight_layout()
        # transformed_sample = transformed_sample.permute(1, 2, 0)
        # plt.imshow(transformed_sample)
        # plt.show()
        images = images.cuda()
        if args.half:
            images = images.half()
        target = target.cuda()

        # compute output
        output,_ = model(images)
        #print(torch.max(torch.softmax(output,dim=-1),dim =-1)[0])
        if args.half:
            output = output.type(torch.cuda.FloatTensor)
            #"""
            #y_soft = F.softmax(output, dim=-1)[0:5,:]
            #print(y_soft.gather(1, torch.unsqueeze(target[0:5], dim=1)))
            #print(criterion(F.softmax(output, dim=-1)[0:5,:],target[0:5]))
            #"""
            #stop
        loss = criterion(output, target)
        #print(loss)
        #print("==========================================")

        # measure accuracy and record loss
        if args.use_arcface_loss:
            acc1, acc5 = accuracy(output[0], target, topk=(1, 5))
        else:
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
        losses.update(loss.item(), images.size(0))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        optimizer.zero_grad()
        loss.backward()
        #print(model.kernel.grad.shape)
        #print(model.kernel.grad[0])
        optimizer.step()


        batch_time.update(time() - begin)
        begin = time()
        if i % args.print_freq == 0:
            progress.display(i)

    return top1.avg, losses.avg

def train_mask(args,train_loader,model,criterion, optimizer,epoch,freeze=False):
    if freeze:
        if args.dp:
            model.module.input_layer.eval()
        else:
            model.input_layer.eval()


    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1   = AverageMeter('Acc@1', ':6.2f')
    mtop1   = AverageMeter('MAcc@1', ':6.2f')

    top5   = AverageMeter('Acc@5', ':6.2f')
    f1_meter   = AverageMeter('F1', ':6.2f')

    progress = ProgressMeter(
        len(train_loader),
        [batch_time,losses, top1,mtop1,f1_meter, top5],
        prefix="Epoch: [{}]".format(epoch))
    begin = time()
    w = torch.FloatTensor([1, 4]).cuda()

    w_ce = torch.nn.CrossEntropyLoss(weight=w)
    for i, (images, target,mask_target) in enumerate(train_loader):

        """
        import cv2
        import numpy as np
        from torchvision import transforms
        img = transforms.ToPILImage()(images[0])
        #img = images[0].cpu().numpy()
        cv2.imshow("photo", img)
        #f = np.rollaxis(np.expand_dims(img, axis=0), 1, 4)

        cv2.waitKey(0)
        """

        images = images.cuda()
        if args.half:
            images = images.half()
        target = target.cuda()
        mask_target = mask_target.cuda()

        # compute output
        output,mask_output,_ = model(images)
        if args.half:
            output = output.type(torch.cuda.FloatTensor)
            mask_output = mask_output.type(torch.cuda.FloatTensor)
        id_loss = criterion(output, target)
        mloss = w_ce(mask_output, mask_target)

        #mloss =  torch.mean(focal_loss(mask_output, mask_target,alpha=0.1,gamma=1.5))
        if not  freeze:
            loss = mloss + id_loss
        else:
            loss = mloss
        # measure accuracy and record loss
        if args.use_arcface_loss:
            acc1, acc5 = accuracy(output[0], target, topk=(1, 5))

        else:
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            true_positives, false_positives, true_negatives, false_negatives,precision,recall,f1 = confusion(mask_output,mask_target)
            macc1        = accuracy(mask_output, mask_target)[0]
            if i % args.print_freq == 0:
                print("tp:",true_positives,"fp:", false_positives,"tn:", true_negatives,"fn:", false_negatives)
        losses.update(loss.item(), images.size(0))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))
        mtop1.update(macc1[0], images.size(0))
        f1_meter.update(f1, images.size(0))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


        batch_time.update(time() - begin)
        begin = time()
        if i % args.print_freq == 0:
            progress.display(i)


    return top1.avg, losses.avg,mtop1.avg