#!/usr/bin/python
import cv2
import os

import tensorflow as tf
import numpy as np
import detect_face
import matplotlib.pylab as plt
import json
from glob import glob
import random


def get_bbox(file_path):
    # try:
    frame = cv2.imread(file_path)
    # import ipdb; ipdb.set_trace(context=30)
    try:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    except:
        return
    frame1 = np.expand_dims(frame, axis=0)
    batch_frames = np.concatenate([frame1] * 1, axis=0)
    bounding_boxes = detect_face.bulk_detect_face(batch_frames, detection_window_size_ratio, pnet, rnet, onet,
                                                  threshold, factor)
    try:
        jsn = {'bbox': bounding_boxes[0][0].tolist(), 'kps': bounding_boxes[0][1].tolist()}
        print(file_path, '--------------------------------')
        # import ipdb; ipdb.set_trace(context=30)
        out_file = os.path.join(file_path.replace('.jpg', '.json'))
        with open(out_file, 'w') as g:
            # with open(file_path+'.json', 'w') as g:
            json.dump(jsn, g)
    except:
        pass

    # except:
    #    print("fail with ",file_path)
    # continue


#   cv2.rectangle(frame,(int(bbox[0]),int(bbox[1])),(int(bbox[2]),int(bbox[3])),(0, 255, 0), 4);
#   plt.imshow(fr
#   TYCOGARAGE
#   ame)
#   plt.show()

if __name__ == '__main__':
    dataset_path = '/home/img_align_celeba/img_align_celeba'  # '/data/combined_western_112_asian_celeb_clipped_with_glasses_and_masks'#
    detection_window_size_ratio = 0.1
    factor = 0.709  # scale factor
    threshold = [0.6, 0.6, 0.6]  # three steps's threshold
    g = tf.Graph()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
    sess = tf.Session(graph=g, config=tf.ConfigProto(gpu_options=gpu_options))  #
    with sess.as_default():
        with g.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(sess,
                                                        '/home/barak/workspace/smart_vision/facenet_master/frozen_graph/frozen_MTCNN.pb')

    # import ipdb; ipdb.set_trace(context=30)
    # with open(file_list_path) as f:
    #    file_list = [x.strip() for x in f]
    # file_list = ['/data/tlv_face/sequences_data/sequences_relabelled_not_cropped/operational_sequences/Visonic_sequece_relabelled_not_cropped_good_db/Unauthorized/10_10_11_29_track_925_person_14_0.004514867179539528']
    file_list = glob(dataset_path +'/*.jpg') #'/**/*.jpg')

    #file_list = random.sample(file_list, int(len(file_list) / 5))
    for fl in file_list:
        #  folder = fl#.rsplit('/',1)[0]
        # import ipdb; ipdb.set_trace(context=30)
        # img_list = [x for x in os.listdir(os.path.join(dataset_path,folder)) if x.endswith('png') ]
        # for img_path in img_list:
        # img_path = os.path.join(dataset_path, fl[:-5]+'.png')
        #     get_bbox(os.path.join(dataset_path,folder,img_path)
        get_bbox(fl)
