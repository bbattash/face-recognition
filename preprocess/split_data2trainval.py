import os
import sys
import subprocess
import collections
import glob
import random
#1,614,761
if __name__ == "__main__":
   path = "/home/glint_30_to_300_variaty_of_masks_3.52m/"
   val_dir = "/home/glint_30_to_300_variaty_of_masks_3.52m_val/"
   for id in glob.glob(path+"/*"):
      val_id = id.split('/')[-1]
      id_val_full_path = os.path.join(val_dir,val_id)
      if not os.path.exists(id_val_full_path):
         os.mkdir(id_val_full_path)
      for img in glob.glob(id+"/*"):
         choice = random.choices([0,1], weights=[98,2])[0] #1- validation
         if choice:
           bashCommand = "sudo cp  " + img + " " + id_val_full_path
           output = subprocess.check_output(['bash', '-c', bashCommand])
           bashCommand = "sudo rm  " + img
           output = subprocess.check_output(['bash', '-c', bashCommand])