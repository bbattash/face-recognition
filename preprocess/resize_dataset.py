import cv2
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
smart_vision_path = os.path.join(os.getcwd(), "../../../../smart_vision/facenet_master/")
sys.path.insert(0, smart_vision_path)
from tycoai_vision.align_trans import warp_and_crop_face, get_reference_facial_points
import json
main_folder = "/home/barak/exact_size"
out_folder = "/home/mask_val_dataset_112"
if not os.path.exists(out_folder):
    os.mkdir(out_folder)
for dir in os.listdir(main_folder):
    dir_path = os.path.join(main_folder,dir)
    o_path = os.path.join(out_folder,dir)
    if not os.path.exists(o_path):
        os.mkdir(o_path)
    for file in os.listdir(dir_path):
        file_path =os.path.join(dir_path,file)
        #file_path = "/home/barak/workspace/face-recognition/mask_augment_creation/face-mask/face_mask/images/reg_mask_1.jpg"
        output_path = os.path.join(o_path,file)
        #output_path = "/home/barak/workspace/face-recognition/mask_augment_creation/face-mask/face_mask/images/reg_mask_1_236_138.jpg"
        if ".JPG" not in file_path and ".png" not in file_path and ".jpg" not in file_path:
            continue

        frame=cv2.imread(file_path)
        frame = cv2.resize(frame,(112,112))
        cv2.imwrite(output_path,frame)

