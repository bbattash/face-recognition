
import os
import time
from time import time

## from torch.utils.tensorboard import SummaryWriter
from torch import optim

from config import get_args
from dataloaders import get_custom_data
from dataloaders import get_train_loader, get_val_data
from loss_objectives import CustomArcface
from model import build_model
from ranger import Ranger
from utils import save_checkpoint, remove_module_if_needed
from validation import validate
from train import train
import torch
from clearml import Task
from minima_prop_utils.utils import get_sharpness,get_nonuniformity
from embedding_extraction import extract_embedding


best_acc1 = 0

def main():
    global best_acc1
    torch.backends.cudnn.benchmark = True

    args = get_args()

    temp = args.milestones.split(',')
    args.milestones= [float(f) for f in temp]
    task = Task.init(project_name="Face Recognition", task_name=args.task_name)
    logger = task.get_logger()
    if  not os.path.exists(args.results_dir):
        os.mkdir(args.results_dir)
    args.save_path = args.results_dir +'/'+ args.save_path+"_lr"+str(args.lr)+"_wd"+str(args.weight_decay)+"_"+args.train_data_folder
    if not os.path.exists(args.save_path) and not args.evaluate:
       os.mkdir(args.save_path)
    if not args.evaluate:
        with open(args.save_path+'/args.txt', 'w') as f:
            for k,v in args.__dict__.items():
                f.write(str(k))
                f.write(':')
                f.write(str(v))
                f.write('\n')
    #model =.cuda()
    args.tb_filename = os.path.join(args.save_path,"tb")
    #tb = SummaryWriter(args.tb_filename)

    if not args.embedding:
        train_loader, val_loader, num_of_classes = get_train_loader(args)

        val_data = get_val_data(args)
        c_val_data=get_custom_data(args.custom_file,args.custom_path)
    #agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df
        args.num_of_classes = num_of_classes
    else:
        args.num_of_classes = 2

    model = build_model(args)
    if args.dp:
        model = torch.nn.DataParallel(model)
    model = model.cuda()

    criterion = CustomArcface(label_smoothing=args.label_smoothing, use_arcface_loss= args.use_arcface_loss,accloss=args.accloss).cuda()
    if args.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)
    elif args.optimizer == 'adam':
        optimizer = optim.Adam([var1, var2], lr=args.lr)
    else:
        #optim.SGD([{'params': [param for name, param in model.named_parameters() if 'fc2' not in name]},
                   #{'params': model.fc2.parameters(), 'lr': 5e-3}], lr=1e-2)
        if args.half:
            optimizer = Ranger(model.parameters(),lr=args.lr, weight_decay=args.weight_decay,eps=1e-4)
        else:
            optimizer = Ranger(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    if args.freeze_bb:
        ft_optimizer = Ranger(model.parameters(),lr=args.lr_ft, weight_decay=args.weight_decay)
        ft_scheduler = torch.optim.lr_scheduler.MultiStepLR(ft_optimizer,gamma=0.1,
                                                     milestones=[10,14,17])

    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer,gamma=args.gamma,
                                                     milestones=args.milestones)

    if args.load_path is not "":
        print("Loading: ",args.load_path)
        if "_ts" in args.load_path:
            model = torch.jit.load(args.load_path)
        else:
            old_dict = torch.load(args.load_path)['state_dict']
            dict = {}
            for k,v in old_dict.items():
                if k =="kernel":
                    k = 'fc.weight'
                    dict[k] = v.transpose_(0, 1)
                else:
                    dict[k]=v
            if not args.dp:
                dict = remove_module_if_needed(dict)

            if args.freeze_bb and not args.ft_mask or  args.remove_cls:
            #if args.remove_cls:
                print("Removing fc.weight")
                if args.dp:
                    del dict['module.fc.weight']
                else:
                    del dict['fc.weight']
            #"""
            for k,v in dict.items():
                #print(k)
                if k=="fc.weight":
                    print(k)
                    print(v[0:1,0:5])
                    print("--------------")
            #"""
            #print(model)
            model.load_state_dict(dict,strict=False)
            print("Loaded succesfuly")

    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']

            best_acc1 = best_acc1.cuda()
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))

        else:
            print("=> no checkpoint found at '{}'".format(args.resume))
    if args.embedding:
        emb_dir = "/home/000001_new"
        extract_embedding(args, emb_dir, model, criterion)
    elif args.evaluate:
        acc1, v_loss, ytf_acc, lfw_acc,custom_acc,_ = validate(args, val_loader, val_data,c_val_data, model, criterion)
        sharpness = get_sharpness(model, criterion, train_loader,   n_iters=5, verbose=True, tol=1e-4)
        #non_uniformity = get_nonuniformity(model, criterion, train_loader, \
        #                                   n_iters=5, verbose=True, tol=1e-4)
    else:
        if args.freeze_bb:
            from utils import freeze_bb
            model = freeze_bb(args,model)
            for epoch in range(1,args.ft_epochs+1):
                train_top1,t_loss = train(args, train_loader, model, criterion, ft_optimizer, epoch,freeze=True)
                acc1,v_loss,ytf_acc,lfw_acc,custom_acc,mask_f1 = validate(args, val_loader,val_data,c_val_data, model, criterion)
                ft_scheduler.step()

                save_checkpoint({
                    'epoch': epoch + 1,
                    'arch': args.arch,
                    'state_dict': model.state_dict(),
                    'best_acc1': best_acc1,
                    'optimizer': optimizer.state_dict(),
                }, True, epoch, args, lfw_acc, ytf_acc, custom_acc, mask_f1, train_top1, acc1)
            model = freeze_bb(args, model,unfreeze=True)
        for epoch in range(args.start_epoch,args.epochs+1):

            start = time()
            train_top1,t_loss = train(args, train_loader, model, criterion, optimizer, epoch)
            acc1,v_loss,ytf_acc,lfw_acc,custom_acc,mask_f1  = validate(args, val_loader,val_data,c_val_data, model, criterion)
            scheduler.step()
            #"""
            logger.report_scalar("Train_Top1", "series A", iteration=epoch, value=train_top1)

            logger.report_scalar("Train_loss", "series A", iteration=epoch, value=t_loss)
            logger.report_scalar("Val_Top1", "series A", iteration=epoch, value=acc1)
            #logger.report_scalar("Val_MTop1", "series A", iteration=epoch, value=macc1)
            #logger.report_scalar("Train_MTop1", "series A", iteration=epoch, value=train_mtop1)


            logger.report_scalar("Val_loss", "series A", iteration=epoch, value=v_loss)
            logger.report_scalar("YTF Acc", "series A", iteration=epoch, value=ytf_acc)
            logger.report_scalar("LFW Acc", "series A", iteration=epoch, value=lfw_acc)
            #"""
            #if args.mask:
            #    logger.report_scalar("Mask F1", "series A", iteration=epoch, value=mask_f1)
            # remember best acc@1 and save checkpoint

            is_best = acc1 > best_acc1
            best_acc1 = max(acc1, best_acc1)
            e_time = time() - start
            print("Epoch time:",e_time)

            save_checkpoint({
                    'epoch': epoch + 1,
                       'arch': args.arch,
                    'state_dict': model.state_dict(),
                    'best_acc1': best_acc1,
                    'optimizer' : optimizer.state_dict(),
                }, is_best,epoch,args,lfw_acc,ytf_acc,custom_acc,mask_f1,train_top1,acc1)


if __name__ == '__main__':
    main()