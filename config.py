import argparse
from pathlib import Path

def get_args():
    parser = argparse.ArgumentParser(description='for face verification')
    parser.add_argument("-e", "--epochs", help="training epochs", default=15, type=int)
    parser.add_argument("-net", "--net_mode", help="which network, [ir, ir_se, mobilefacenet]",default='ir_se', type=str)
    parser.add_argument("-depth", "--net_depth", help="how many layers [50,100,152]", default=50, type=int)
    parser.add_argument("-print_freq", help="The frequency of batchs printing to the screen", default=100, type=int)

    parser.add_argument('--lr',help='learning rate',default=0.018, type=float)
    parser.add_argument('--lr_ft',help='learning rate for finetuning',default=0.1, type=float)

    parser.add_argument("-b", "--batch_size", help="batch_size", default=80, type=int)
    parser.add_argument("-w", "--num_workers", help="Number of workers for dataloader", default=2, type=int)
    parser.add_argument("--verify_images", help="Perform verification on training images", action='store_true')
    parser.add_argument("--load_model_from_ckpt", help="Load the model from previously trained checkpoint", action='store_false')
    parser.add_argument("--label_smoothing", help="Use label smoothing reqularization",
                        action='store_false')
    parser.add_argument("--find_lr", help="Apply find learning rate algorithm before training", action='store_true')
    parser.add_argument('--val_pct', help='Percent of validation data from training set', default=0.02, type=float)
    parser.add_argument('--start_epoch', help='', default=1, type=int)

    parser.add_argument("--arch", help="Backbone architecture to use for training", default='mxresnet50',#mxresnet50',
                        type=str)#se_resnet50
    parser.add_argument("--task_name", help="task name for allegro", default='bestmodel.pth',
                        type=str)

    parser.add_argument("--train_data_folder", help="Training data folder", default='msceleb_rgb_cropped',#combined_western_112_asian_celeb_clipped',
                        type=str)#mini_train, combined_western_112_asian_celeb_clipped/
    parser.add_argument("--tensorboard_path", help="Path to tensorboard log", default='data/tensorboard/',
                        type=str)
    parser.add_argument("--val_path", help="Path to validation data", default='/home/val_data',
                        type=str)
    parser.add_argument("--main_data_folder", help="Root data folder", default=Path('/home/'))
    parser.add_argument("--embedding_size", help="Embedding size", default=512, type=int)
    parser.add_argument('--momentum', help='Optimizer momentum', default=0.9, type=float)
    parser.add_argument('--weight_decay', help='Optimizer weight decay', default=5e-4, type=float)
    parser.add_argument("--optimizer", help="ada/sgd/ranger", default='ranger',type=str)

    parser.add_argument("--use_mlflow", help="Use mlflow", action='store_false')
    parser.add_argument("--use_arcface_loss", help="Use Arcface (store_false) or regular CE loss", action='store_true')
    parser.add_argument("--accloss", help="Use Arcface (store_false) or regular CE loss", action='store_true')
    parser.add_argument("--sched", help="Schedualing the lr decrease, barak_sched or base_sched", default="base_sched")
    parser.add_argument("--load_path", help="This is for loading a precise weights",
                        default="")
    parser.add_argument("--results_dir", help="This is for loading a precise weights",
                        default="./results")
    parser.add_argument("--mask_val_path", default="")
    parser.add_argument("--save_path",default ="")
    parser.add_argument("--milestones",default ="10,14,17")
    parser.add_argument("--ft_epochs",type=int,default =3)
    parser.add_argument("--dropout",type=float,default =0.2)
    parser.add_argument("--gamma",type=float,default =0.1)

    parser.add_argument("--re", help="Use random erase", action='store_true')

    parser.add_argument("--cutmix", help="Use cutmix", action='store_true')
    parser.add_argument("--dp", help="Dataparallel", action='store_true')
    parser.add_argument("--evaluate",action='store_true')
    parser.add_argument("--embedding",action='store_true')
    parser.add_argument("--bald_cls",action='store_true')

    parser.add_argument("--freeze_bb", help="trains only the classifier ", action='store_true')
    parser.add_argument("--mask", help="train with mask ", action='store_true')

    parser.add_argument("--resume", help="path to resume",default="")

    parser.add_argument('--accum_steps', help='gradient accuulation step', default=1, type=int)
    parser.add_argument('--custom_file',  default="/home/val_data/custom_full_new.txt", type=str)
    parser.add_argument('--custom_path',  default="/home/val_data/own_val_dataset_new", type=str)
    parser.add_argument("--ft_mask", help="ft mask classifier ", action='store_true')
    parser.add_argument("--q", help="Quantize model", action='store_true')
    parser.add_argument("--ensamble", help="Use ensamble model", action='store_true')

    parser.add_argument("--half", help="Use half precison", action='store_true')
    parser.add_argument("--save_as_1_4", help="Save in pytorch 1.4 format", action='store_true')
    parser.add_argument("--remove_cls", help="remove classifier", action='store_true')
    parser.add_argument("--aux", help="remove classifier", action='store_true')
    parser.add_argument("--random_prespective", help="Use random prespective aug", action='store_true')
    parser.add_argument("--shift_and_pad", help="Use shift and pad aug", action='store_true')
    parser.add_argument("--random_crop", help="RESIZE and then crop", action='store_true')

    parser.add_argument("--uniq_ckpt_name", help="task name for allegro", default="",
                        type=str)
    #####################################################################################
    parser.add_argument('--projector', default='8192-8192-8192', type=str,
                        metavar='MLP', help='projector MLP')
    parser.add_argument('--lambd', default=3.9e-3, type=float, metavar='L',
                        help='self-supervision training weight on off-diagonal terms')
    parser.add_argument('--scale-loss', default=1 / 32, type=float,
                        metavar='S', help='scale the loss')
    #################################################################################
                        #"bestmodel_combined_western_112_asian_celeb_clipped_LS_True_backbone_mxresnet50_opt_ranger_base_sched_2020-09-23_16:47:10.248312_lr_0.00018_64.6withmask_3.pth")#"bestmodel_combined_western_112_asian_celeb_clipped_LS_True_backbone_mxresnet50_opt_ranger_99_4.pth")
    #parser.add_argument('--start_arcface_from_epoch', help='from which epoch to start using arcface and not ce,1000 is never', default=1000, type=int)

    args = parser.parse_args()
    return args
