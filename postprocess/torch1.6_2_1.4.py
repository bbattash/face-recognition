import torch

path= "/home/barak/workspace/face-recognition/mask_focus_results/asiando0.4_aboveglint_30to300_mask_re_lr0.0001_wd0.01_clean_asian_celeb_trimmed_112_mask/asian_do0.4_glint_30to300_mask_re_checkpoint_10_99.75_96.12_91.25_97.18_96.63.pth"
out_path = path[:-4]+"_1.4.pth"
dict = torch.load(path)
torch.save(dict,out_path,_use_new_zipfile_serialization=False)