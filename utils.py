import torch
import os
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)
        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))
        res = []
        for k in topk:
            try:
                correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            except:
                correct_k = correct[:k].contiguous().view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res

import shutil
def save_checkpoint(state, is_best, epoch, args,lfw_acc,ytf_acc,custom_acc,mask_f1,train_top1,acc1):
    if args.mask:
        filename = os.path.join(args.save_path,args.uniq_ckpt_name +"_checkpoint_"+str(epoch)+"_"+str(round(lfw_acc*100,2))+"_"+str(round(ytf_acc*100,2))+"_"+str(round(custom_acc*100,2))+"_maskf1_"+str(round(mask_f1,3))+"_"+str(round(train_top1.item(),2)) +"_"+str(round(acc1.item(),2)) +".pth")
    else:
        filename = os.path.join(args.save_path,args.uniq_ckpt_name +"_checkpoint_" + str(epoch) + "_" + str(round(lfw_acc * 100, 2)) + "_" + str(round(ytf_acc * 100, 2)) + "_" + str(round(custom_acc * 100, 2))+"_"+str(round(train_top1.item(),2))+"_"+str(round(acc1.item(),2)) + ".pth")

    best = os.path.join(args.save_path, "best.pth")
    if args.save_as_1_4:
        torch.save(state, filename, _use_new_zipfile_serialization=False)
    else:
        torch.save(state, filename)

    if is_best:
        shutil.copyfile(filename, best)
def save_checkpoint_base(state,filename):
    torch.save(state, filename)


def freeze_bb(args,model,unfreeze=False,mask=False):

    if not unfreeze:
        if args.dp:
            for param in model.module.input_layer.parameters():
                param.requires_grad = False
            if mask:
                model.module.kernel.requires_grad = False

        else:
            for param in model.input_layer.parameters():
                    param.requires_grad = False
            if mask:
                model.kernel.requires_grad = False
    else:
        if args.dp:
            for param in model.module.input_layer.parameters():
                param.requires_grad = True
            if mask:
                model.module.kernel.requires_grad = True
        else:
            for param in model.input_layer.parameters():
                param.requires_grad = True
            if mask:
                model.kernel.requires_grad = True
    return model

def remove_module_if_needed(dict):
    from collections import OrderedDict
    new_state_dict = OrderedDict()

    for k, v in dict.items():
        if "module" in k:
            name = k[7:] # remove `module.`
        else:
            name=k
        new_state_dict[name] = v
    return new_state_dict

from glob import glob
def load_ensamble_with_weights(model):
    paths = glob('bald_recognition_*')
    dict = torch.load(paths[0])['state_dict']
    model.net1.load_state_dict(dict,strict=True)
    dict = torch.load(paths[1])['state_dict']
    model.net2.load_state_dict(dict,strict=True)
    dict = torch.load(paths[2])['state_dict']
    model.net3.load_state_dict(dict,strict=True)
    dict = torch.load(paths[3])['state_dict']
    model.net4.load_state_dict(dict,strict=True)
    dict = torch.load(paths[4])['state_dict']
    model.net5.load_state_dict(dict,strict=True)

    print("Ensamble loaded succesfuly")
    return model