import pretrainedmodels

#from models.mxresnet import *
from models.se_ir_model import Backbone
from models.tresnet import TResnetM,TResnetL
from torch import nn
import torch
import math
from models.mxresnet import Mish
from models.ensamble import  Ensamble
from torch.nn import functional as F
class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

class AuxBackbone(nn.Module):
    def __init__(self,args, input_layer, classnum=1, m=0.5, use_arcface_loss=True):
        super(AuxBackbone, self).__init__()
        self.arcface = use_arcface_loss
        #self.arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='mish', p=args.dropout)

        self.input_layer = input_layer
        self.fc1 = nn.Linear(512, 2)
        self.fc2 = nn.Linear(512, 2)
        self.fc3 = nn.Linear(512, 2)
        self.fc4 = nn.Linear(512, 2)
        self.fc5 = nn.Linear(512, 2)
        self.fc6 = nn.Linear(512, 2)
        self.fc7 = nn.Linear(512, 2)
        self.fc8 = nn.Linear(512, 2)

    def forward(self, x):
        x = self.input_layer(x)
        emb_out = x
        embeddings = l2_norm(x)
        out1 = self.fc1(embeddings)
        out2 = self.fc2(embeddings)
        out3 = self.fc3(embeddings)
        out4 = self.fc4(embeddings)
        out5 = self.fc5(embeddings)
        out6 = self.fc6(embeddings)
        out7 = self.fc7(embeddings)
        out8 = self.fc8(embeddings)
        return (out1,out2,out3,out4,out5,out6,out7,out8)
class BaldBackbone(nn.Module):
    def __init__(self,args, input_layer, classnum=1, m=0.5, use_arcface_loss=True):
        super(BaldBackbone, self).__init__()
        self.arcface = use_arcface_loss
        #self.arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='mish', p=args.dropout)
        self.input_layer = input_layer
        self.fc = nn.Linear(512, 64,bias=False)
        self.fc1 = nn.Linear(64, 2,bias=False)

    def forward(self, x):

        x = self.input_layer(x)
        emb_out = x
        embeddings = l2_norm(x)
        out = self.fc(embeddings)
        out = F.leaky_relu(out)
        out = self.fc1(out)
        return out,emb_out
class MaskBackbone(nn.Module):
    def __init__(self,args, input_layer, classnum=1, m=0.5, use_arcface_loss=True):
        super(MaskBackbone, self).__init__()
        self.arcface = use_arcface_loss
        #self.arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='mish', p=args.dropout)

        self.input_layer = input_layer
        #self.kernel = nn.Parameter(torch.Tensor(512, classnum))  # .cuda()
        # initial kernel
        #self.kernel.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)
        self.m = m  # the margin value, default is 0.5
        self.cos_m = math.cos(m)
        self.sin_m = math.sin(m)
        self.mm = self.sin_m * m  # issue 1
        self.threshold = math.cos(math.pi - m)
        self.output_layer = None
        #self.mask_do = nn.Dropout(0.7)
        self.mask_fc = nn.Linear(512,64)
        self.mask_fc1 = nn.Linear(64,2)
        self.fc = nn.Linear(512,classnum , bias=False)

    def forward(self, x):
        x = self.input_layer(x)
        #x = self.arcface_head(x)
        emb_out = x
        embeddings = l2_norm(x)
        if not self.arcface:
            #print(self.fc.weight[0:1,0:5])
            #stop
            output_layer = self.fc(embeddings)
        else:
            kernel_norm = l2_norm(self.kernel, axis=0)
            cos_theta = torch.mm(embeddings, kernel_norm)
            cos_theta = cos_theta.clamp(-1, 1)  # for numerical stability
            cos_theta_2 = torch.pow(cos_theta, 2)
            sin_theta_2 = 1 - cos_theta_2
            sin_theta = torch.sqrt(sin_theta_2)
            cos_theta_m = (cos_theta * self.cos_m - sin_theta * self.sin_m)
            # this condition controls the theta+m should in range [0, pi]
            #      0<=theta+m<=pi
            #     -m<=theta<=pi-m
            cond_v = cos_theta - self.threshold
            cond_mask = cond_v <= 0
            keep_val = (cos_theta - self.mm)  # when theta not in [0,pi], use cosface instead
            cos_theta_m[cond_mask] = keep_val[cond_mask]
            self.output_layer = cos_theta, cos_theta_m
        #do_emb = self.mask_do(embeddings)
        mask_out = F.relu(self.mask_fc(embeddings))
        mask_out = self.mask_fc1(mask_out)
        return output_layer,mask_out, emb_out

class Backbone(nn.Module):
    def __init__(self, input_layer, classnum=1, m=0.5, use_arcface_loss=True):
        super(Backbone, self).__init__()
        self.arcface = use_arcface_loss
        self.input_layer = input_layer
        #self.kernel = nn.Parameter(torch.Tensor(512, classnum))#.cuda()
        # initial kernel
        #self.kernel.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)
        self.m = m  # the margin value, default is 0.5
        self.cos_m = math.cos(m)
        self.sin_m = math.sin(m)
        self.mm = self.sin_m * m  # issue 1
        self.threshold = math.cos(math.pi - m)
        self.output_layer = None

        self.fc = nn.Linear(512,classnum,bias=False)



    def forward(self, x):

        x = self.input_layer(x)
        emb_out =x
        embeddings = l2_norm(x)
        if not self.arcface:
            output_layer = self.fc(embeddings)

        else:
            kernel_norm = l2_norm(self.kernel, axis=0)
            cos_theta = torch.mm(embeddings, kernel_norm)
            cos_theta = cos_theta.clamp(-1, 1)  # for numerical stability
            cos_theta_2 = torch.pow(cos_theta, 2)
            sin_theta_2 = 1 - cos_theta_2
            sin_theta = torch.sqrt(sin_theta_2)
            cos_theta_m = (cos_theta * self.cos_m - sin_theta * self.sin_m)
            # this condition controls the theta+m should in range [0, pi]
            #      0<=theta+m<=pi
            #     -m<=theta<=pi-m
            cond_v = cos_theta - self.threshold
            cond_mask = cond_v <= 0
            keep_val = (cos_theta - self.mm)  # when theta not in [0,pi], use cosface instead
            cos_theta_m[cond_mask] = keep_val[cond_mask]
            self.output_layer = cos_theta, cos_theta_m

        return output_layer,emb_out

class AdaptiveConcatPool2d(nn.Module):
    "Layer that concats `AdaptiveAvgPool2d` and `AdaptiveMaxPool2d`."
    def __init__(self, sz=None):
        super(AdaptiveConcatPool2d,self).__init__()
        "Output will be 2*sz or 2 if sz is None"
        self.output_size = sz or 1
        self.ap = nn.AdaptiveAvgPool2d(self.output_size)
        self.mp = nn.AdaptiveMaxPool2d(self.output_size)

    def forward(self, x): return torch.cat([self.mp(x), self.ap(x)], 1)
def bn_drop_lin(n_in, n_out, bn=True, p=0., actn=None):
    "Sequence of batchnorm (if `bn`), dropout (with `p`) and linear (`n_in`,`n_out`) layers followed by `actn`."
    layers = [nn.BatchNorm1d(n_in)] if bn else []
    if p != 0: layers.append(nn.Dropout(p))
    layers.append(nn.Linear(n_in, n_out))
    if actn is not None: layers.append(actn)
    return layers
def create_head_arcface(nf, nc, lin_ftrs=None, p=0.5,
                concat_pool=True, bn_final=False, actn=None):
    "Model head that takes `nf` features, runs through `lin_ftrs`, and about `nc` classes."
    lin_ftrs = [nf, 512, nc] if lin_ftrs is None else [nf] + lin_ftrs + [nc]

    if actn == 'mish' :
        actns = [Mish()] * (len(lin_ftrs) - 2) + [None]
    else:
        actns = [nn.LeakyReLU(inplace=True)] * (len(lin_ftrs)-2) + [None]
        #actns = [nn.ReLU(inplace=True)] * (len(lin_ftrs)-2) + [None]
    pool = AdaptiveConcatPool2d() if concat_pool else nn.AdaptiveAvgPool2d(1)
    layers = [pool, Flatten()]
    bn_required = len(lin_ftrs) - 2
    for ni,no,actn in zip(lin_ftrs[:-1], lin_ftrs[1:], actns):
        layers += bn_drop_lin(ni, no, True, p, actn)
        if bn_required > 0:
            bn_required = bn_required - 1
            layers[-2] = nn.Linear(layers[-2].in_features, layers[-2].out_features, bias=False)

    if bn_final:
        layers[-1] = nn.Linear(layers[-1].in_features, layers[-1].out_features, bias= False)
        layers.append(nn.BatchNorm1d(lin_ftrs[-1], momentum=0.01))
    network = nn.Sequential(*layers)
    return network


def l2_norm(input,axis=1):
    norm = torch.norm(input,2,axis,True)
    output = torch.div(input, norm)
    return output

class Arcface(nn.Module):
    # implementation of additive margin softmax loss in https://arxiv.org/abs/1801.05599
    def __init__(self, embedding_size=512, classnum=51332,  s=64., m=0.5):
        super(Arcface, self).__init__()
        self.classnum = classnum
        self.kernel = nn.Parameter(torch.Tensor(embedding_size,classnum))
        # initial kernel
        self.kernel.data.uniform_(-1, 1).renorm_(2,1,1e-5).mul_(1e5)
        self.m = m # the margin value, default is 0.5
        self.s = s # scalar value default is 64, see normface https://arxiv.org/abs/1704.06369
        self.cos_m = math.cos(m)
        self.sin_m = math.sin(m)
        self.mm = self.sin_m * m  # issue 1
        self.threshold = math.cos(math.pi - m)
    def forward(self, embbedings, label):
        # weights norm
        nB = len(embbedings)
        kernel_norm = l2_norm(self.kernel,axis=0)
        # cos(theta+m)
        cos_theta = torch.mm(embbedings,kernel_norm)
#         output = torch.mm(embbedings,kernel_norm)
        cos_theta = cos_theta.clamp(-1,1) # for numerical stability
        cos_theta_2 = torch.pow(cos_theta, 2)
        sin_theta_2 = 1 - cos_theta_2
        sin_theta = torch.sqrt(sin_theta_2)
        cos_theta_m = (cos_theta * self.cos_m - sin_theta * self.sin_m)
        # this condition controls the theta+m should in range [0, pi]
        #      0<=theta+m<=pi
        #     -m<=theta<=pi-m
        cond_v = cos_theta - self.threshold
        cond_mask = cond_v <= 0
        keep_val = (cos_theta - self.mm) # when theta not in [0,pi], use cosface instead
        cos_theta_m[cond_mask] = keep_val[cond_mask]
        output = cos_theta * 1.0 # a little bit hacky way to prevent in_place operation on cos_theta
        idx_ = torch.arange(0, nB, dtype=torch.long)
        output[idx_, label] = cos_theta_m[idx_, label]
        output *= self.s # scale up in order to make softmax work, first introduced in normface
        return output

def get_model(pretrained=True, model_name = 'se_resnet50', **kwargs ):

    if model_name == 'tresnet_m':
        arch = TResnetM()
    elif model_name == 'ensamble':
        arch = Ensamble()
    elif model_name == 'tresnet_l':
        arch = TResnetL()

    elif model_name == 'se_ir':
        arch = Backbone(50, 0.6, 'ir_se')
        dict= torch.load("model_ir_se50_not_10.pth")
        arch.load_state_dict(dict,strict=True)
    else:
        if pretrained:
            arch = pretrainedmodels.__dict__[model_name](num_classes=1000, pretrained='imagenet')
        else:
            arch = pretrainedmodels.__dict__[model_name](num_classes=1000, pretrained=None)
    return arch

def build_model(args):
    if args.arch == "se_ir":
        model = get_model(model_name=args.arch)
    else:

        body = list(get_model(model_name=args.arch).children())
        if 'mxresnet' in args.arch or 'tresnet' in args.arch:

            if args.arch == "tresnet_l":
                if args.half:
                    arcface_head = create_head_arcface(4864, args.embedding_size, bn_final=True, actn='lrelu',
                                                       p=args.dropout)
                else:
                    arcface_head = create_head_arcface(4864, args.embedding_size, bn_final=True, actn='mish',
                                                           p=args.dropout)
            else:
                if args.half:
                    arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='lrelu',
                                                       p=args.dropout)
                else:
                    #arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='mish',p=args.dropout)
                    arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='lrelu',
                                                           p=args.dropout)
        else:
            arcface_head = create_head_arcface(4096, args.embedding_size,p=args.dropout)


        model = nn.Sequential(*body[:-2], arcface_head)
        if args.mask:
            model = MaskBackbone(args,model, classnum=args.num_of_classes, use_arcface_loss=args.use_arcface_loss)
        elif args.bald_cls:
            model = BaldBackbone(args, model, classnum=args.num_of_classes, use_arcface_loss=args.use_arcface_loss)
        elif args.aux:
            model = AuxBackbone(args, model, classnum=args.num_of_classes, use_arcface_loss=args.use_arcface_loss)
        else:
            model = Backbone(model, classnum=args.num_of_classes, use_arcface_loss= args.use_arcface_loss)
    return model


def build_model_entropy(args):
    if args.arch == "se_ir":
        model = get_model(model_name=args.arch)
    else:
        body = list(get_model(model_name=args.arch).children())
        if 'mxresnet' in args.arch or 'tresnet' in args.arch:
            if args.arch == "tresnet_l":
                arcface_head = create_head_arcface(4864, args.embedding_size, bn_final=True, actn='mish',
                                                       p=args.dropout)
            else:
                arcface_head = create_head_arcface(4096, args.embedding_size, bn_final=True, actn='mish',p=args.dropout)

        else:
            arcface_head = create_head_arcface(4096, args.embedding_size,p=args.dropout)


        model = nn.Sequential(*body[:-2], arcface_head)
        if args.mask:
            model = MaskBackbone(args,model, classnum=131386, use_arcface_loss=args.use_arcface_loss)
            #model = MaskBackbone(args,*body[:-2], classnum=args.num_of_classes, use_arcface_loss=args.use_arcface_loss)

        else:
            model = Backbone(model, classnum=131386, use_arcface_loss= args.use_arcface_loss)
    return model


class Ensamble(nn.Module):
    def __init__(self,args):
        super(Ensamble, self).__init__()
        self.net1 = build_model(args)
        self.net2 = build_model(args)
        self.net3 = build_model(args)
        self.net4 = build_model(args)
        self.net5 = build_model(args)

    def forward(self,x):
        out1 = self.net1(x)[0]
        out2 = self.net2(x)[0]
        out3 = self.net3(x)[0]
        out4 = self.net4(x)[0]
        out5 = self.net5(x)[0]

        out =out1 + out2+out3+out4+out5
        return out/7