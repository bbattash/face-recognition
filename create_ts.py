from model import build_model
from config import get_args
import torch
import sys
from models.tresnet_utils.export_utils import InplacABN_to_ABN,fuse_bn_recursively
from model import build_model
from config import get_args
import torch
import sys
from torch import nn
from models.tresnet_utils.export_utils import InplacABN_to_ABN,fuse_bn_recursively
from train import cast_running_stats
##without pth
model_name ="/home/barak/workspace/face-recognition/mask_focus_results/L_mask_half_asianmask_above_glint_40_300_mask_lr1e-05_wd0.01_clean_asian_celeb_trimmed_112_mask/L_mask_half_asianmask_above_glint_40_300_mask_do0.4_checkpoint_1_99.67_96.55_91.24_maskf1_0_97.33_98.38"
args = get_args()
dict = torch.load(model_name+'.pth',map_location='cuda:1')['state_dict']
"""
for k,v in  dict.items():
    print(k)
stop
"""

args.num_of_classes = dict['kernel'].shape[1]
"""
from collections import OrderedDict
new_state_dict = OrderedDict()
for k, v in dict.items():
    if "module" in k:
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
# load params
"""

m_=build_model(args)
#for k,v in torch.load(model_name+'.pth','cuda')['model'].items():
#  print(k)
m_.load_state_dict(dict,strict=True)
if args.half:
  m_.half()
  m_.apply(cast_running_stats)

  for layer in m_.modules():
    if isinstance(layer, nn.BatchNorm2d):
      layer.float()
m_ = InplacABN_to_ABN(m_)
m_ = fuse_bn_recursively(m_)
if args.mask:
  m1=m_.cuda()
else:
  m1=torch.nn.Sequential(*m_.input_layer).cuda()

m1.eval()
z=torch.zeros((1,3,112,112,)).cuda()
if args.half:
  z = z.half()
traced_gpu = torch.jit.trace(m1, (z))
torch.jit.save(traced_gpu, model_name+"_ts")
m2 = torch.jit.load( model_name+"_ts")

z=torch.zeros((1,3,112,112,)).cuda()
with torch.no_grad():
  print(m2(z)[0][0])


