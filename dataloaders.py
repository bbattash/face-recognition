from  torchvision import transforms
from pathlib import Path
import os,sys
smart_vision_path = os.path.join(os.getcwd(), "../../../smart_vision/facenet_master/")
sys.path.insert(0, smart_vision_path)
from tycoai_vision.align_trans import warp_and_crop_face, get_reference_facial_points
from torch.utils.data import Dataset, ConcatDataset, DataLoader
from torchvision import transforms as trans
from torchvision.datasets import ImageFolder,DatasetFolder
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
import numpy as np
import cv2
import bcolz
import pickle
import torch
#import mxnet as mx
from tqdm import tqdm
import os
import pandas as pd
import fnmatch
from home_made_trasforms import ShiftAndPad

def get_ytf_data(data_path):
    manifest = pd.read_csv(data_path+'/ytf/youtube_faces_with_keypoints_full.csv')  # 2194 rows
    imgs = []
    for root, _, filenames in os.walk(data_path+'/ytf/images'):
        for filename in fnmatch.filter(filenames, '*.npz'):
            imgs.append(os.path.join(root, filename))

    imgs = pd.DataFrame(imgs)
    imgs.columns = ['file_name']
    imgs = pd.DataFrame([(i, j) for i in manifest.videoID for j in imgs.file_name if i in j])
    imgs.columns = ['videoID', 'file_name']
    #ytf_norm_bgr = torch.zeros([len(imgs.index), conf.embedding_size], device=conf.device)

    for row in range(int(len(imgs.index))):
        f = np.load(imgs.iloc[row]['file_name'])
        vid = f['colorImages']
        bb = f['boundingBox']
        landmarks = f['landmarks2D']
        #frame_sample = int(vid.shape[3] / 2)
        for frame_sample in range(0,min(40,vid.shape[3]-1)):
            frame = np.reshape(vid[:, :, :, frame_sample], (1, vid.shape[0], vid.shape[1], vid.shape[2]))
            frame_bb = bb[:,:,frame_sample]
            ##Take sampled frame and extract 5 kps out of 68
            kps_temp = landmarks[:, :, frame_sample]

            frame_kps = (kps_temp[46, :] + kps_temp[43, :]) / 2
            frame_kps = frame_kps.reshape((1, 2))
            temp = (kps_temp[37, :] + kps_temp[40, :]) / 2
            temp = temp.reshape((1, 2))
            frame_kps=np.concatenate((frame_kps,temp) ,axis = 0 )
            temp = kps_temp[34, :].reshape((1, 2))
            frame_kps=np.concatenate((frame_kps, temp),axis = 0 )
            temp = kps_temp[49, :].reshape((1, 2))
            frame_kps=np.concatenate((frame_kps, temp),axis = 0 )
            temp = kps_temp[5, :].reshape((1, 2))
            frame_kps=np.concatenate((frame_kps, temp),axis = 0 )
            #frame_kps = landmarks[:, :, frame_sample]
            #frame = crop(frame,frame_bb)
            frame  = warp_and_crop_face(frame, frame_kps,
                               get_reference_facial_points(default_square=True), crop_size=(112,112),export='image')
            if frame_sample == 0:
                if len(frame.shape) == 4:
                    new_vid = frame
                else:
                    new_vid = np.expand_dims(frame,axis=0)#.cpu()
            else:
                if len(frame.shape) == 3:
                    new_vid = np.concatenate((new_vid,np.expand_dims(frame,axis=0)),axis=0)
                else:
                    new_vid = np.concatenate((new_vid,frame),axis=0)#np.expand_dims(frame,axis=0)),axis=0)
        if row == 0:
            ytf_norm_bgr = np.expand_dims(new_vid,axis=0)#.cpu()
        else:
            ytf_norm_bgr = np.concatenate((ytf_norm_bgr,np.expand_dims(new_vid,axis=0)),axis=0)

    return  ytf_norm_bgr,imgs


def de_preprocess(tensor):
    return tensor * 0.5 + 0.5

def get_custom_data(custom_file,custom_path):
    import cv2
    with open(custom_file,'r') as file:
        issame=[]
        for i,line in  enumerate(file):
            first,sec,isame =line.split(',')
            first,sec = os.path.join(custom_path,first),os.path.join(custom_path,sec)
            f = np.rollaxis(np.expand_dims(cv2.imread(first),axis=0), 3, 1)
            s = np.rollaxis(np.expand_dims(cv2.imread(sec),axis=0), 3, 1)
            img = np.concatenate((f,s),axis=0)
            if i ==0:
                so_far =img
            else:
                so_far = np.concatenate((so_far,img),axis=0)
            issame.append(int(isame))
    issame = np.array(issame)
    custom_data = (np.asarray(so_far, dtype=np.float32) * 0.5) + 0.5
    lfw_rgb = custom_data[:, ::-1, :, :] - np.zeros_like(custom_data)
    mean_lfw = 0.5
    std_lfw  = 0.5
    for i in range(3):
        custom_data[:, i, :, :] = (custom_data[:, i, :, :] - mean_lfw) / std_lfw
    return issame, custom_data

def get_train_dataset(args,imgs_folder):
    # if args.ft_mask:
    #     train_transform = trans.Compose([
    #         trans.RandomHorizontalFlip(),
    #         trans.ColorJitter(),
    #         trans.RandomAffine(0.2),
    #         trans.ToTensor(),
    #         trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
    #         transforms.RandomErasing(p=0.5, scale=(0.05, 0.2))
    #     ])
    # else:
    if args.re and args.shift_and_pad:
        #print("I'm using random erase")
        train_transform = trans.Compose([
        trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        transforms.RandomApply([ShiftAndPad()], p=0.5),
        transforms.RandomErasing(p=0.5, scale=(0.05, 0.2))
        ])
    elif args.re :
        #print("I'm using random erase")
        train_transform = trans.Compose([
        trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        transforms.RandomErasing(p=0.5, scale=(0.05, 0.2))
        ])
    elif args.random_prespective:
        #print("I'm using random prespective")
        train_transform = trans.Compose([
        transforms.RandomApply([trans.RandomPerspective(distortion_scale=0.05)],p=0.2),
        trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
        ])
    elif args.random_crop:
        print("Using random crop")
        #print("I'm using random prespective")
        train_transform = trans.Compose([
        #trans.RandomCrop(112),
        trans.RandomCrop(112),
        #trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
        ])
    elif args.shift_and_pad:
        print("I'm using shift anf pad")
        train_transform = trans.Compose([
        trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        transforms.RandomApply([ShiftAndPad()], p=0.5),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
        ])
    else:
        print("I'm not using any spaciel aug!!!")
        train_transform = trans.Compose([
        #trans.RandomHorizontalFlip(),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        ])

    if args.mask:
        ds = ImageFolderMaskLabel(imgs_folder, transform=train_transform)
    else:
        ds = ImageFolder(imgs_folder, transform=train_transform)
    class_num = ds[-1][1] + 1

    return ds, class_num

def get_val_dataset(args, imgs_folder):
    val_transform = trans.Compose([
       # trans.Resize(112),
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])
    if args.mask:
        ds = ImageFolderMaskLabel(imgs_folder, transform=val_transform)
    else:
        ds = ImageFolder(imgs_folder, transform=val_transform)
    return ds
# from torch import randperm
# from torch._utils import _accumulate
# def random_split(dataset, lengths):
#
#     if sum(lengths) != len(dataset):
#         raise ValueError("Sum of input lengths does not equal the length of the input dataset!")
#     indices = randperm(sum(lengths)).tolist()
#     return [Subset(dataset, indices[offset - length:offset]) for offset, length in zip(_accumulate(lengths), lengths)]

def get_train_loader(args):
    train_ds, class_num,train_transform = get_train_dataset(args,args.main_data_folder / args.train_data_folder)
    val_ds, class_num, val_transform = get_train_dataset(args,
                                                                            args.main_data_folder / args.train_data_folder+"_val")
    """
    split1 = int(len(ds) * 0.98)
    split2 = len(ds) - split1
    splits = (split1,split2)
    train_ds,val_ds = torch.utils.data.random_split(ds,splits)
    # insert transforms to train_ds and val_ds
    val_ds.dataset.transform = val_transform
    train_ds.dataset.transform = train_transform
    
    print(train_ds.dataset.transform)
    """
    train_loader = DataLoader(train_ds, batch_size=args.batch_size, shuffle=True,# pin_memory=args.pin_memory,
                        num_workers=args.num_workers)
    val_loader = DataLoader(val_ds, batch_size=args.batch_size, shuffle=True,# pin_memory=args.pin_memory,
                              num_workers=args.num_workers)
    return train_loader,val_loader, class_num


def get_train_loader_aux(args):
    path2celeba = "/home/img_align_celeba/celeba_crroped"
    if args.re:
        train_transform = trans.Compose([
            trans.RandomHorizontalFlip(),
            trans.ToTensor(),
            trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            transforms.RandomErasing(p=0.5, scale=(0.05, 0.2))
        ])
    else:
        train_transform = trans.Compose([
            #trans.RandomHorizontalFlip(),
            trans.ToTensor(),
            trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        ])
    ds = CelebaDataset(path2celeba,train_transform)
    class_num = 2
    val_transform = trans.Compose([
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])
    split1 = int(len(ds) * 0.98)
    split2 = len(ds) - split1
    splits = (split1,split2)
    train_ds,val_ds = torch.utils.data.random_split(ds,splits)
    # insert transforms to train_ds andbrt val_ds
    val_ds.dataset.transform = val_transform
    train_loader = DataLoader(train_ds, batch_size=args.batch_size, shuffle=True,# pin_memory=args.pin_memory,
                        num_workers=args.num_workers)
    val_loader = DataLoader(val_ds, batch_size=args.batch_size, shuffle=False,# pin_memory=args.pin_memory,
                              num_workers=args.num_workers)

    return train_loader,val_loader, class_num



def get_val_pair(path, name):
    carray = bcolz.carray(rootdir=path +'/'+ name, mode='r')
    issame = np.load(path +'/'+ '{}_list.npy'.format(name))
    carray = (np.asarray(carray) * 0.5) + 0.5
    carray = carray[:, ::-1, :, :] - np.zeros_like(carray)
    mean = 0.5
    std  = 0.5
    for i in range(3):
        carray[:, i, :, :] = (carray[:, i, :, :] - mean) / std
    return carray, issame
def get_val_mask_loader(args):
    val_transform = trans.Compose([
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])
    ds = ImageFolderWithPath(args.mask_val_path, transform=val_transform)
    mval_loader = DataLoader(ds, batch_size=1, shuffle=True,# pin_memory=args.pin_memory,
                              num_workers=args.num_workers)
    return mval_loader
def get_val_data(args):
    data_path = args.val_path
    agedb_30, agedb_30_issame = get_val_pair(data_path, 'agedb_30')
    cfp_fp, cfp_fp_issame = get_val_pair(data_path, 'cfp_fp')
    lfw, lfw_issame = get_val_pair(data_path, 'lfw')

    if not os.path.exists(data_path+'/ytf/ytf_b4_norm.npz'):
        ytf_rgb, imgs_df = get_ytf_data(data_path)
        np.savez(data_path+'/ytf/ytf_b4_norm.npz',ytf_rgb)
        imgs_df.to_csv(data_path+"/ytf/imgs_df.csv")
    else:
        ytf_rgb = np.load(data_path+'/ytf/ytf_b4_norm.npz')['arr_0']
        imgs_df = pd.read_csv(data_path+"/ytf/imgs_df.csv")
    if args.mask_val_path:
        mask_val_loader = get_val_mask_loader(args)
        return agedb_30, cfp_fp, lfw,ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame,imgs_df,mask_val_loader
    else:
        return agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df

IMG_EXTENSIONS = ('.jpg', '.jpeg', '.png', '.ppm', '.bmp', '.pgm', '.tif', '.tiff', '.webp')

def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        img = Image.open(f)
        return img.convert('RGB')
def default_loader(path):
    from torchvision import get_image_backend
    return pil_loader(path)
class ImageFolderMaskLabel(ImageFolder):

    # override the __getitem__ method. this is the method that dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns
        original_tuple = super(ImageFolderMaskLabel, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        last_name = path.split('/')[-1]
        if "mask" in last_name:
            mask_label = 1
        else:
            mask_label = 0
        # make a new tuple that includes original and the path
        img,label =original_tuple
        tuple_with_path = (original_tuple + (mask_label,))
        return         tuple_with_path

        # (img,(label,mask_label))

def get_inference_loader(args):
    transform = trans.Compose([
    trans.ToTensor(),
    trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
    ])

    ds = ImageFolderWithPath(args.main_data_folder / args.train_data_folder, transform=transform)
    class_num = 2

    ds.transform = transform

    loader = DataLoader(ds, batch_size=args.batch_size, shuffle=True,# pin_memory=args.pin_memory,
                        num_workers=args.num_workers)

    return loader, class_num
class ImageFolderWithPath(ImageFolder):

    # override the __getitem__ method. this is the method that dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns
        original_tuple = super(ImageFolderWithPath, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        last_name = path.split('/')[-1]


        tuple_with_path = (original_tuple + (path,))
        return         tuple_with_path


class CelebaDataset(Dataset):
    def __init__(self, img_dir, transform=None):
        txt_path = "/home/img_align_celeba/list_attr_celeba.csv"
        df = pd.read_csv(txt_path, sep=",", index_col=0)
        self.img_dir = img_dir
        self.txt_path = txt_path
        self.img_names = df.index.values
        self.y_male          = df['Male'].values
        self.y_male[self.y_male==-1] = 0

        self.y_narrow_eyes   = df['Narrow_Eyes'].values
        self.y_narrow_eyes[self.y_narrow_eyes == -1] = 0

        self.y_arched_eyebrows =  df['Arched_Eyebrows'].values
        self.y_arched_eyebrows[self.y_arched_eyebrows == -1] = 0

        self.y_big_nose        = df['Big_Nose'].values
        self.y_big_nose[self.y_big_nose == -1] = 0

        self.y_big_lips = df['Big_Lips'].values
        self.y_big_lips[self.y_big_lips == -1] = 0

        self.y_bald = df['Bald'].values
        self.y_bald[self.y_bald == -1] = 0
        #unique, counts = np.unique(self.y_bald, return_counts=True)
        #print(dict(zip(unique, counts)))
        #stop


        self.y_eyeglasses = df['Eyeglasses'].values
        self.y_eyeglasses[self.y_eyeglasses == -1] = 0

        self.y_pale_skin = df['Pale_Skin'].values
        self.y_pale_skin[self.y_pale_skin == -1] = 0

        self.transform = transform
        #8 classes


    def __getitem__(self, index):
        path = os.path.join(self.img_dir,self.img_names[index])
        while not os.path.exists(path):
            index += 1
            path = os.path.join(self.img_dir, self.img_names[index])
        img = Image.open(path)

        if self.transform is not None:
            img = self.transform(img)

        label_male            = self.y_male[index]
        label_narrow_eyes     = self.y_narrow_eyes[index]
        label_arched_eyebrows = self.y_arched_eyebrows[index]
        label_big_nose        = self.y_big_nose[index]
        label_big_lips        = self.y_big_lips[index]
        label_bald            = self.y_bald[index]
        label_eyeglasses      = self.y_eyeglasses[index]
        label_pale_skin       = self.y_pale_skin[index]
        labels = (label_male,label_narrow_eyes,label_arched_eyebrows,label_big_nose,label_big_lips,label_bald,label_eyeglasses,label_pale_skin)
        return img, labels

    def __len__(self):
        return self.y_male.shape[0]
