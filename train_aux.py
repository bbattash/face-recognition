import torch
from utils import AverageMeter,ProgressMeter,accuracy
import tqdm
from torch import nn
from time import time
#from kornia.losses import focal_loss
from evaluation_modules import confusion
from inplace_abn import InPlaceABN
from torch.nn import functional as F
torch.autograd.set_detect_anomaly(True)
def cast_running_stats(m):
    if isinstance(m, InPlaceABN):
        m.running_mean = m.running_mean.float()
        m.running_var = m.running_var.float()


def train(args,train_loader,model,criterion, optimizer,epoch,freeze=False):

    if freeze:
        if args.dp:
            model.module.input_layer.eval()
        else:
            model.input_layer.eval()

    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    top1_male   = AverageMeter('Acc_male@1', ':6.2f')
    top1_narrow_eyes = AverageMeter('Acc@1_narrow_eyes', ':6.2f')
    top1_arched_eyebrows = AverageMeter('Acc_arched_eyebrows@1', ':6.2f')
    top1_big_nose = AverageMeter('Acc_big_nose@1', ':6.2f')
    top1_big_lips = AverageMeter('Acc_big_lips@1', ':6.2f')
    top1_bald = AverageMeter('Acc_bald@1', ':6.2f')
    top1_eyeglasses = AverageMeter('Acc_eyeglasses@1', ':6.2f')
    top1_pale_skin = AverageMeter('Acc_pale_skin@1', ':6.2f')
    tops = [top1_male,top1_narrow_eyes,top1_arched_eyebrows,top1_big_nose,top1_big_lips,top1_bald,top1_eyeglasses,top1_pale_skin]
    progress = ProgressMeter(
        len(train_loader),
        [batch_time,losses, top1_male,top1_narrow_eyes,top1_arched_eyebrows,top1_big_nose,top1_big_lips,top1_bald,top1_eyeglasses,top1_pale_skin],
        prefix="Epoch: [{}]".format(epoch))

    begin = time()
    torch.set_printoptions(edgeitems=10)

    for i, (images, targets) in enumerate(train_loader):

        for label_idx in range(len(targets)):
            targets[label_idx] =targets[label_idx].cuda()
        #label_male,label_narrow_eyes,label_arched_eyebrows,label_big_nose,label_big_lips,label_bald,label_eyeglasses,label_pale_skin = targets

        images = images.cuda()
        if args.half:
            images = images.half()


        # compute output
        output = model(images)
        if args.half:
            output = output.type(torch.cuda.FloatTensor)

        loss = criterion(output[0], targets[0])
        acc1 = accuracy(output[0], targets[0])[0]
        tops[0].update(acc1[0], images.size(0))
        for label_idx in range(1,len(targets)):
            loss += criterion(output[label_idx], targets[label_idx])
            acc1 = accuracy(output[label_idx], targets[label_idx])[0]
            tops[label_idx].update(acc1[0], images.size(0))

        losses.update(loss.item(), images.size(0))



        optimizer.zero_grad()
        loss.backward()
        #print(model.kernel.grad.shape)
        #print(model.kernel.grad[0])
        optimizer.step()


        batch_time.update(time() - begin)
        begin = time()
        if i % args.print_freq == 0:
            progress.display(i)

    #return top1.avg, losses.avg


def val(args,val_loader,model,criterion, optimizer,epoch,freeze=False):
    model.eval()
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    top1_male   = AverageMeter('Acc_male@1', ':6.2f')
    top1_narrow_eyes = AverageMeter('Acc@1_narrow_eyes', ':6.2f')
    top1_arched_eyebrows = AverageMeter('Acc_arched_eyebrows@1', ':6.2f')
    top1_big_nose = AverageMeter('Acc_big_nose@1', ':6.2f')
    top1_big_lips = AverageMeter('Acc_big_lips@1', ':6.2f')
    top1_bald = AverageMeter('Acc_bald@1', ':6.2f')
    top1_eyeglasses = AverageMeter('Acc_eyeglasses@1', ':6.2f')
    top1_pale_skin = AverageMeter('Acc_pale_skin@1', ':6.2f')
    tops = [top1_male,top1_narrow_eyes,top1_arched_eyebrows,top1_big_nose,top1_big_lips,top1_bald,top1_eyeglasses,top1_pale_skin]
    progress = ProgressMeter(
        len(val_loader),
        [batch_time,losses, top1_male,top1_narrow_eyes,top1_arched_eyebrows,top1_big_nose,top1_big_lips,top1_bald,top1_eyeglasses,top1_pale_skin],
        prefix="Epoch: [{}]".format(epoch))

    begin = time()
    torch.set_printoptions(edgeitems=10)

    for i, (images, targets) in enumerate(val_loader):

        for label_idx in range(len(targets)):
            targets[label_idx] =targets[label_idx].cuda()
        #label_male,label_narrow_eyes,label_arched_eyebrows,label_big_nose,label_big_lips,label_bald,label_eyeglasses,label_pale_skin = targets

        images = images.cuda()
        if args.half:
            images = images.half()


        # compute output
        with torch.no_grad():
            output = model(images)
        if args.half:
            output = output.type(torch.cuda.FloatTensor)

        loss = criterion(output[0], targets[0])
        for label_idx in range(1,len(targets)):
            loss += criterion(output[label_idx], targets[label_idx])
            acc1 = accuracy(output[label_idx], targets[label_idx])[0]
            tops[label_idx].update(acc1[0], images.size(0))

        losses.update(loss.item(), images.size(0))






        batch_time.update(time() - begin)
        begin = time()
        if i % args.print_freq == 0:
            progress.display(i)

    #return top1.avg, losses.avg
####################################################################################################
import torch
from utils import AverageMeter,ProgressMeter,accuracy
import tqdm
from torch import nn
from time import time
#from kornia.losses import focal_loss
from evaluation_modules import confusion
from inplace_abn import InPlaceABN
from torch.nn import functional as F
torch.autograd.set_detect_anomaly(True)
def cast_running_stats(m):
    if isinstance(m, InPlaceABN):
        m.running_mean = m.running_mean.float()
        m.running_var = m.running_var.float()


def train_bald(args,train_loader,model,criterion, optimizer,epoch,freeze=False):

    if freeze:
        if args.dp:
            model.module.input_layer.eval()
        else:
            model.input_layer.eval()

    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    top1   = AverageMeter('Acc_bald@1', ':6.2f')

    progress = ProgressMeter(
        len(train_loader),
        [batch_time,losses, top1],
        prefix="Epoch: [{}]".format(epoch))

    begin = time()
    torch.set_printoptions(edgeitems=10)

    for i, (images, targets) in enumerate(train_loader):

        target =targets[-3].cuda()

        images = images.cuda()
        if args.half:
            images = images.half()


        # compute output
        output,_ = model(images)
        if args.half:
            output = output.type(torch.cuda.FloatTensor)

        loss = criterion(output, target)
        acc1 = accuracy(output, target)[0]
        top1.update(acc1[0], images.size(0))

        losses.update(loss.item(), images.size(0))

        optimizer.zero_grad()
        loss.backward()
        #print(model.kernel.grad.shape)
        #print(model.kernel.grad[0])
        optimizer.step()


        batch_time.update(time() - begin)
        begin = time()
        if i % args.print_freq == 0:
            progress.display(i)

    #return top1.avg, losses.avg

from sklearn.metrics import confusion_matrix
def val_bald(args,val_loader,model,criterion, optimizer,epoch,freeze=False):
    model.eval()
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')

    top1   = AverageMeter('Acc_bald@1', ':6.2f')
    tp_ = AverageMeter('TP@1', ':6.2f')
    fp_ = AverageMeter('FP@1', ':6.2f')
    fn_ = AverageMeter('FN@1', ':6.2f')
    tn_ = AverageMeter('TN@1', ':6.2f')
    f1_ = AverageMeter('F1@1', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time,losses, top1,f1_,tp_,fp_,fn_,tn_],
        prefix="Epoch:      [{}]".format(epoch))

    begin = time()
    torch.set_printoptions(edgeitems=10)

    for i, (images, targets) in enumerate(val_loader):


        target =targets[-3].cuda()
        #print("Num of true:",torch.sum(target))
        images = images.cuda()
        if args.half:
            images = images.half()


        # compute output

        with torch.no_grad():
            if args.ensamble:
                output = model(images)
            else:
                output, _ = model(images)

            #print(F.softmax(output))
        if args.half:
            output = output.type(torch.cuda.FloatTensor)

        loss = criterion(output, target)
        threshold_vec = torch.cuda.FloatTensor([0.8,0])

        output=F.softmax(output,dim=-1)+threshold_vec

        y_pred = torch.argmax(output,dim=1).cpu()

        #print(y_pred.shape)
        tn, fp, fn, tp = confusion_matrix(target.cpu(),y_pred).ravel()
        precision = tp / (tp + fp + 1e-7)
        recall = tp / (tp + fn + 1e-7)

        f1 = 2 * (precision * recall) / (precision + recall + 1e-7)

        #print(tn, fp, fn, tp)
        losses.update(loss.item(), images.size(0))

        acc1 = accuracy(output, target)[0]
        top1.update(acc1[0], images.size(0))
        tp_.update(tp, images.size(0))
        tn_.update(tn, images.size(0))
        fp_.update(fp, images.size(0))
        fn_.update(fn, images.size(0))
        f1_.update(f1, images.size(0))
        losses.update(loss.item(), images.size(0))

        batch_time.update(time() - begin)
        begin = time()

    progress.display(i)

def inference(args, val_loader, model):
    model.eval()
    id_arr =[]
    for i, (images, targets,paths) in enumerate(val_loader):

        target = targets[-3].cuda()
        images = images.cuda()
        if args.half:
            images = images.half()
        # compute output
        with torch.no_grad():
            if args.ensamble:
                output = model(images)
            else:
                output, _ = model(images)
        if args.half:
            output = output.type(torch.cuda.FloatTensor)

        for path,sample in zip(paths,output):
            if torch.softmax(sample,dim=-1)[1].item()>0.99:
                #3print(path)
                id = path.split('/')[-2]
                # print(id,id_arr)
                if id not in id_arr:
                     id_arr.append(id)
                # if len(id_arr) > 15:
                #     stop
                with open("bald_guys_0.99.txt",'a+') as f:
                    #f.write("/home/glint_40_to_300_variaty_of_masks/id_"+str(id)+"/"+path)

                    f.write(path)
                    f.write('\n')
    print("len of id arr:",len(id_arr))