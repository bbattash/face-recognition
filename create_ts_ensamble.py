
from model import build_model
from config import get_args
import torch
import sys
import math
from models.tresnet_utils.export_utils import InplacABN_to_ABN,fuse_bn_recursively
##without pth
class ParallelModule(torch.nn.Sequential):
  def __init__(self, *args):
    super(ParallelModule, self).__init__(*args)

  def forward(self, input):
    output = []
    sum = torch.zeros([512]).cuda()
    i = 0
    for module in self:
      i+=1
      sum = torch.add(sum, module(input))
    output= sum/i
    return output
args = get_args()

model_name1 ="/home/barak/workspace/face-recognition/results/asianmask_on_glintmask_on_glint_lr0.01_wd0.01_clean_asian_celeb_trimmed_112_mask/asianmask_on_glintmask_checkpoint_9_99.43_93.65_91.59_100.0_98.95_1.6"
model_name2 ="/home/barak/workspace/face-recognition/results/asianmask_on_glintmask_on_glint_lr0.01_wd0.01_clean_asian_celeb_trimmed_112_mask/asianmask_on_glintmask_checkpoint_9_99.43_93.65_91.59_100.0_98.95_1.6"
model_name3 ="/home/barak/workspace/face-recognition/results/asianmask_on_glintmask_on_glint_lr0.01_wd0.01_clean_asian_celeb_trimmed_112_mask/asianmask_on_glintmask_checkpoint_9_99.43_93.65_91.59_100.0_98.95_1.6"
dict1 = torch.load(model_name1+'.pth',map_location='cuda:1')['state_dict']
dict2 = torch.load(model_name2+'.pth',map_location='cuda:1')['state_dict']
dict3 = torch.load(model_name3+'.pth',map_location='cuda:1')['state_dict']

"""
for k,v in  dict.items():
    print(k)
stop
"""

args.num_of_classes = dict1['kernel'].shape[1]
"""
from collections import OrderedDict
new_state_dict = OrderedDict()
for k, v in dict.items():
    if "module" in k:
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
# load params
"""
m1_=build_model(args)
m2_=build_model(args)
m3_=build_model(args)
#for k,v in torch.load(model_name+'.pth','cuda')['model'].items():
#  print(k)
m1_.load_state_dict(dict1,strict=True)
m1_ = InplacABN_to_ABN(m1_)
m1_ = fuse_bn_recursively(m1_)
m2_.load_state_dict(dict2,strict=True)
m2_ = InplacABN_to_ABN(m2_)
m2_ = fuse_bn_recursively(m2_)
m3_.load_state_dict(dict3,strict=True)
m3_ = InplacABN_to_ABN(m3_)
m3_ = fuse_bn_recursively(m3_)

if args.mask:
  m1=m1_.cuda()
else:
  m1=torch.nn.Sequential(*m1_.input_layer).cuda()
  m2=torch.nn.Sequential(*m2_.input_layer).cuda()
  m3=torch.nn.Sequential(*m3_.input_layer).cuda()

m = ParallelModule(m1,m2,m3)
m.eval()
z=torch.zeros((1,3,112,112,)).cuda()
traced_gpu = torch.jit.trace(m, (z))
torch.jit.save(traced_gpu, "ensamble_ts")
m2 = torch.jit.load( "ensamble_ts")

z=torch.zeros((1,3,112,112,)).cuda()
with torch.no_grad():
  print(m2(z))


