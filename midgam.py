import subprocess
import os
from  builtins import any as b_any
if __name__ == "__main__":
    path = "/home/glint_40_to_300_masks"
    out_path = "/home/midgam"
    if not os.path.exists(out_path):
        os.mkdir(out_path)
    for dir in os.listdir(path):
        if "csv" in dir:
            continue
        dir_path = os.path.join(path,dir)
        lst = os.listdir(dir_path)
        img_path = os.path.join(dir_path,lst[0])
        mask_path = subprocess.check_output("cp " + img_path + " " + out_path, shell=True)



