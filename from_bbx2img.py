import xml.etree.ElementTree as ET
import os
import torch
from bs4 import BeautifulSoup
import cv2
def getname(obj):
    if obj.find('name').text()=="with_mask":
        name = 1
    else:
        name=0
def get_box(obj):
    xmin = int(obj.find('xmin').text)
    ymin = int(obj.find('ymin').text)
    xmax = int(obj.find('xmax').text)
    ymax = int(obj.find('ymax').text)

    return [xmin, ymin, xmax, ymax]



if __name__ == "__main__":
    path = "/home/mask_data/annotations/"
    images_path= "/home/mask_data/images_1/"
    out_mask = "/home/mask_data/cropped/mask/"
    out_no_mask = "/home/mask_data/cropped/nomask/"
    if not os.path.exists(out_mask):
        os.mkdir(out_mask)
    if not os.path.exists(out_no_mask):
        os.mkdir(out_no_mask)
    mask_counter = 0
    reg_counter = 0
    for file in os.listdir(path):
        full_path = os.path.join(path,file)
        root = ET.parse(full_path).getroot()
        image_path = images_path + "/" + root[1].text
        name_arr=[]
        for k in root.iter('object'):
            name_arr.append(k.find('name').text)
        for idx,bndbox in enumerate(root.iter('bndbox')):
            [xmin, ymin, xmax, ymax] = get_box(bndbox)
            img = cv2.imread(image_path)
            face_img = img[ymin:ymax, xmin:xmax]
            face_img = cv2.resize(face_img, (112, 112))

            if name_arr[idx] == "with_mask":
                cv2.imwrite(out_mask+"/mask_"+str(mask_counter)+".jpg",face_img)
                mask_counter+=1
            else:
                cv2.imwrite(out_no_mask+"/nomask_"+str(reg_counter)+".jpg",face_img)
                reg_counter+=1
        #pass