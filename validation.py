import torch
from utils import AverageMeter,ProgressMeter,accuracy
import tqdm
from time import time
import numpy as np
from model import l2_norm
import pandas as pd
from torchvision import transforms
from evaluation_modules import evaluate
from kornia.losses import focal_loss
from evaluation_modules import confusion
from inplace_abn import InPlaceABN
from torch import nn
def cast_running_stats(m):
    if isinstance(m, InPlaceABN):
        m.running_mean = m.running_mean.float()
        m.running_var = m.running_var.float()
def evaluate_ytf(model, args, carray, imgs_df, nrof_folds=5):
    idx = 0
    carray_temp = np.moveaxis(carray, 4, 2)
    carray_tensor = torch.zeros(carray_temp.shape)
    mean = [0.5, 0.5, 0.5]  # [0.485, 0.456, 0.406]
    std = [0.5, 0.5, 0.5]  # [0.229, 0.224, 0.225]
    for i in range(0, carray.shape[0] - 1):
        for j in range(0, carray.shape[1] - 1):
            carray_tensor[i, j, :, :, :] = transforms.Normalize(mean=mean, std=std)(transforms.ToTensor()(carray[i, j, :, :, :]))
    # if args.dp:
    #    embeddings_ytf = torch.zeros([len(carray_tensor),int(carray_tensor.shape[1]/2), args.embedding_size], device=args.device)
    # else:
    embeddings_ytf = torch.zeros([len(carray_tensor), carray_tensor.shape[1], 512]).cuda()
    i = 0

    with torch.no_grad():
        while i < len(carray_tensor):
            batch = carray_tensor[i, :, :, :, :].cuda()  # .cuda()
            if args.half:
                batch  = batch.half()
            if args.mask:
                _,_, embeddings_ytf[i, :, :] = model(batch)
            else:
                _, embeddings_ytf[i, :, :] = model(batch)

            i += 1

    embeddings_ytf = l2_norm(embeddings_ytf, axis=2).cpu().numpy()
    imgs_dict_temp = imgs_df['videoID'].to_dict()
    imgs_dict = {y: x for x, y in imgs_dict_temp.items()}
    first = True
    df = pd.read_csv(args.val_path+'/ytf/splits_fixed.csv')
    counter_not_in = 0
    issame = []
    for index, row in df.iterrows():
        try:
            index1 = imgs_dict[row['first name']]
            index2 = imgs_dict[row['second name']]
            same = row['is same:']
            issame.append(same)
            if first:
                embeddings1 = np.expand_dims(embeddings_ytf[index1, :], axis=0)
                embeddings2 = np.expand_dims(embeddings_ytf[index2, :], axis=0)
                first = False
            else:
                embeddings1 = np.concatenate((embeddings1, np.expand_dims(embeddings_ytf[index1, :], axis=0)), axis=0)
                embeddings2 = np.concatenate((embeddings2, np.expand_dims(embeddings_ytf[index2, :], axis=0)), axis=0)
        except:
            counter_not_in = counter_not_in + 1

    tpr, fpr, ytf_accuracy, best_thresholds, accuracy_nist = evaluate(embeddings1, embeddings2, issame, nrof_folds)
    ytf_accuracy = ytf_accuracy.mean()
    print("YTF - Acc:", round(ytf_accuracy,4)," Nist Acc:", round(accuracy_nist.mean(),4))
    return ytf_accuracy, best_thresholds.mean()


def evaluate_custom(model, args, carray, issame,ds_name, nrof_folds=5):
    idx = 0

    len_carray = len(carray)
    cur_bs = args.batch_size
    embeddings = torch.zeros([len_carray, 512]).cuda()

    with torch.no_grad():
        while idx + cur_bs <= len_carray:
            batch = torch.tensor(carray[idx:idx + args.batch_size]).cuda()
            if args.half:
                batch  = batch.half()
            if args.mask:
                _, _,embeddings[idx:idx + cur_bs] = model(batch)
            else:
                _, embeddings[idx:idx + cur_bs] = model(batch)
            idx += args.batch_size#cur_bs

        if idx < len_carray:
            batch = torch.tensor(carray[idx:]).cuda()
            if args.half:
                batch  = batch.half()
            if args.mask:
                _,_, embeddings[idx:] = model(batch)
            else:
                _, embeddings[idx:] = model(batch)

    embeddings_lfw = l2_norm(embeddings).cpu().numpy()
    tpr, fpr, lfw_accuracy, best_thresholds,accuracy_nist = evaluate(embeddings_lfw, None, issame, nrof_folds)
    lfw_accuracy = lfw_accuracy.mean()
    print(ds_name+": Acc:",round(lfw_accuracy,4)," Nist Acc::", round(accuracy_nist.mean(),4))


    return lfw_accuracy, best_thresholds.mean()#, roc_curve_tensor
def validate(args,val_loader,val_data,c_val_data, model, criterion):
    if args.half:
        model.half()
        model.apply(cast_running_stats)

        for layer in model.modules():
            if isinstance(layer,nn.BatchNorm2d):
                layer.float()
    if args.mask:
        acc1,v_loss,ytf_acc,lfw_acc,custom_acc,f1 = validate_mask(args,val_loader,val_data,c_val_data, model, criterion)
    else:
        acc1,v_loss,ytf_acc,lfw_acc,custom_acc= validate_nomask(args,val_loader,val_data,c_val_data, model, criterion)
        f1 = 0
    return acc1,v_loss,ytf_acc,lfw_acc,custom_acc,f1
def validate_nomask(args,val_loader,val_data,c_val_data, model, criterion):
    batch_time = AverageMeter('Time', ':6.3f')

    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')

    progress = ProgressMeter(
        len(val_loader),
        [batch_time,losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()
    if args.mask_val_path:
        agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df, mask_val_loader = val_data
    else:
        agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df = val_data
    cissame, custom_data=c_val_data
    # mlflow.log_metric("Mean LFW validation", run_call
    custom_acc,_=evaluate_custom(model,args, custom_data, cissame,'custum')
    lfw_acc,_= evaluate_custom(model,args, lfw, lfw_issame,'lfw')
    ytf_acc,_ = evaluate_ytf(model, args, ytf_rgb, imgs_df)
    with torch.no_grad():
        begin = time()
        for i, (images, target) in enumerate(val_loader):
            images = images.cuda()
            if args.half:
                images = images.half()
            target = target.cuda()

            # compute output


            output,_ = model(images)
            if args.half:
                output = output.type(torch.cuda.FloatTensor)
            loss = criterion(output, target)

            # measure accuracy and record loss
            if args.use_arcface_loss:
                acc1, acc5 = accuracy(output[0], target, topk=(1, 5))
            else:
                acc1, acc5 = accuracy(output, target, topk=(1, 5))

            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time() - begin)
            begin = time()
            if i % args.print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))


    return top1.avg,losses.avg,ytf_acc,lfw_acc,custom_acc

def validate_mask(args,val_loader,val_data,c_val_data, model, criterion):
    batch_time = AverageMeter('Time', ':6.3f')

    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    mtop1 = AverageMeter('MAcc@1', ':6.2f')
    f1_meter   = AverageMeter('F1', ':6.2f')

    progress = ProgressMeter(
        len(val_loader),
        [batch_time,losses, top1,mtop1,f1_meter, top5],
        prefix='Test: ')
    if args.mask_val_path:
        agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df, mask_val_loader = val_data
    else:
        agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df = val_data
    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        cissame, custom_data=c_val_data
        # mlflow.log_metric("Mean LFW validation", run_call
        custom_acc,_=evaluate_custom(model,args, custom_data, cissame,'custum')
        begin = time()
        for i, (images, target,mask_target) in enumerate(val_loader):
            images = images.cuda()
            target = target.cuda()
            mask_target = mask_target.cuda()
            if args.half:
                images = images.half()
            # compute output
            output, mask_output, _ = model(images)
            if args.half:
                output = output.type(torch.cuda.FloatTensor)
                mask_output = mask_output.type(torch.cuda.FloatTensor)
            id_loss = criterion(output, target)
            mloss = torch.mean(focal_loss(mask_output, mask_target, alpha=0.25))
            loss = mloss + id_loss

            # measure accuracy and record loss
            if args.use_arcface_loss:
                acc1, acc5 = accuracy(output[0], mask_target, topk=(1, 5))

            else:
                #print(torch.argmax(mask_output[:10,:],dim=-1),mask_target[:10])
                acc1, acc5 = accuracy(output, target, topk=(1, 5))
                true_positives, false_positives, true_negatives, false_negatives, precision, recall, f1 = confusion(
                    mask_output, mask_target)
                macc1 = accuracy(mask_output, mask_target)[0]
                if i % args.print_freq == 0:
                    print("tp:", true_positives, "fp:", false_positives, "tn:", true_negatives, "fn:", false_negatives)
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))
            mtop1.update(macc1[0], images.size(0))
            f1_meter.update(f1, images.size(0))

            # measure elapsed time
            batch_time.update(time() - begin)
            begin = time()
            if i % args.print_freq == 0:
                progress.display(i)
            # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
                  .format(top1=top1, top5=top5))
        if args.mask_val_path:

            #mtop1 = AverageMeter('TP@1', ':6.2f')
            #f1_meter = AverageMeter('F1', ':6.2f')
            mtop1 = AverageMeter('MAcc@1', ':6.2f')
            tp_meter = AverageMeter('TP', ':6.2f')
            fp_meter = AverageMeter('FP', ':6.2f')
            fn_meter = AverageMeter('FN', ':6.2f')
            tn_meter = AverageMeter('TN', ':6.2f')


            progress = ProgressMeter(
                len(mask_val_loader),
                [ mtop1, tp_meter,fp_meter,fn_meter,tn_meter],
                prefix='Test-Mask: ')
            for i, (images, mask_target,path) in enumerate(mask_val_loader):
                images = images.cuda()
                if args.half:
                    images = images.half()
                mask_target = mask_target.cuda()
                # compute output
                output, mask_output, _ = model(images)
                if args.half:
                    output = output.type(torch.cuda.FloatTensor)
                    mask_output = mask_output.type(torch.cuda.FloatTensor)

                true_positives, false_positives, true_negatives, false_negatives, _,_,_ = confusion(
                    mask_output, mask_target)

                macc1 = accuracy(mask_output, mask_target)[0]
                #print(macc1,path)
                #print("tp:", true_positives, "fp:", false_positives, "tn:", true_negatives, "fn:", false_negatives)

                mtop1.update(macc1[0], images.size(0))
                tp_meter.update(true_positives, images.size(0))
                fp_meter.update(false_positives, images.size(0))
                fn_meter.update(false_negatives, images.size(0))
                tn_meter.update(true_negatives, images.size(0))

                # measure elapsed time
                batch_time.update(time() - begin)
                begin = time()
                if i % args.print_freq == 0:
                    progress.display(i)
            # TODO: this should also be done with the ProgressMeter
            epsilon=1e-7
            print("TP:",tp_meter.avg,"TN:",tn_meter.avg,"FP:",fp_meter.avg,"FN:",fn_meter.avg)
            precision = tp_meter.avg / (tp_meter.avg + fp_meter.avg + epsilon)
            recall = tp_meter.avg / (tp_meter.avg + fn_meter.avg + epsilon)
            f1 = 2 * (precision * recall) / (precision + recall + epsilon)
            print(' * MAcc@1 {top1.avg:.3f} f1_meter {top5:.3f},precision {p:.3f},recall {r:.3f}'
                  .format(top1=mtop1, top5=f1,p=precision,r=recall))



        lfw_acc,_= evaluate_custom(model,args, lfw, lfw_issame,'lfw')
        ytf_acc,_ = evaluate_ytf(model, args, ytf_rgb, imgs_df)
    if args.mask_val_path:
        return top1.avg,losses.avg,ytf_acc,lfw_acc,custom_acc,f1#,mtop1.avg
    else:
        return top1.avg, losses.avg, ytf_acc, lfw_acc, custom_acc,0  # ,mtop1.avg