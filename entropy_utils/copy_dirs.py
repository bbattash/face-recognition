import os
import subprocess
if __name__ == "__main__":
    path_dir= "/home/clean_asian_celeb_trimmed_112"
    out_dir = '/home/glint_30_to_300_combined_asian/'  # +dir
    with open("/home/barak/workspace/face-recognition/entropy_per_id.txt",'r') as f:
        lines =  f.readlines()
        for line in lines:
            name,score = line.strip().split(':')
            if float(score) >9.0:
                print(name)
                path_dir_name = os.path.join(path_dir,name)
                bashCommand = "sudo cp -r " + path_dir_name + " " + out_dir
                output = subprocess.check_output(['bash', '-c', bashCommand])
