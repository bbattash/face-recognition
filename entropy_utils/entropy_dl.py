import torch
from torchvision import datasets
from torch.utils.data import Dataset, ConcatDataset, DataLoader
from torchvision import transforms as trans
from torchvision.datasets import ImageFolder
from PIL import Image, ImageFile
class ImageFolderWithPaths(datasets.ImageFolder):
    """Custom dataset that includes image file paths. Extends
    torchvision.datasets.ImageFolder
    """

    # override the __getitem__ method. this is the method that dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns
        original_tuple = super(ImageFolderWithPaths, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        # make a new tuple that includes original and the path
        tuple_with_path = (original_tuple + (path,))
        return tuple_with_path

def get_val_dataset(imgs_folder):
    val_transform = trans.Compose([
        trans.ToTensor(),
        trans.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
    ])
    ds = ImageFolderWithPaths(imgs_folder, transform=val_transform)
    class_num = ds[-1][1] + 1


    return ds, class_num,val_transform


def get_val_loader(args):


    ds, class_num,val_transform = get_val_dataset(args.main_data_folder / args.train_data_folder)


    val_loader = DataLoader(ds, batch_size=args.batch_size, shuffle=True,# pin_memory=args.pin_memory,
                              num_workers=args.num_workers)

    return val_loader ,class_num