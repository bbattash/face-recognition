import torch
from utils import AverageMeter,ProgressMeter,accuracy
import tqdm
from time import time
import numpy as np
from model import l2_norm
import pandas as pd
from torchvision import transforms
from evaluation_modules import evaluate
from torch import nn
from torch.nn import functional as F
class entropy(nn.Module):
    def __init__(self):
        super(entropy, self).__init__()

    def forward(self, x):
        b = F.softmax(x, dim=1) * F.log_softmax(x, dim=1)
        b = -1.0 * torch.sum(b,dim=-1)
        return  b
def validate(args,val_loader, model):
    batch_time = AverageMeter('Time', ':6.3f')

    # switch to evaluate mode
    model.eval()
    entropy_func =entropy()
    #check_avg_entropy -  means to check the avg entorpy of a dataset to grasp from that data what is the right threshold
    check_avg_entropy=False
    avg = 0
    each_class_dict = {}
    with torch.no_grad():
        for i, (images, target,paths) in enumerate(val_loader):
            images = images.cuda()
            target = target.cuda()

            # compute output
            output,_ = model(images)
            e_out = entropy_func(output)
            #print("entropy of:",path[0]," is:",e_out)
            if check_avg_entropy:
                mean = torch.mean(e_out).item()
                print(mean)
                avg += mean
            else:
                for m,path in enumerate(paths):
                    id = path.split('/')[-2]
                    print(e_out[m].item())
                    if id not in each_class_dict:
                        each_class_dict[id]=[e_out[m].item()]
                    else:
                        each_class_dict[id].append(e_out[m].item())
    max=0
    min=10000
    if check_avg_entropy:
        print("Avg_entropy:",avg/i)
    else:
        with open("entropy_per_id.txt", 'w+') as f:
            for k,v in each_class_dict.items():
                print(k)
                avg = round(sum(v)/len(v),4)
                if avg>max:
                    max=avg
                    print("New max:",avg)
                elif avg<min:
                    min = avg
                    print("New min:",avg)

                f.write(k)
                f.write(':')
                f.write(str(round(avg,3)))
                f.write('\n')







