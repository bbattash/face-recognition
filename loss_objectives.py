import torch
from torch import nn
from torch.nn import functional as F

class ArcfaceFastAI(nn.Module):
    # implementation of additive margin softmax loss in https://arxiv.org/abs/1801.05599
    def __init__(self, s=64.):
        super(ArcfaceFastAI, self).__init__()
        self.s = s  # scalar value default is 64, see normface https://arxiv.org/abs/1704.06369
    def forward(self, input, label):
        cos_theta, cos_theta_m = input
        # weights norm
        nB = len(cos_theta_m)
        output = cos_theta * 1.0  # a little bit hacky way to prevent in_place operation on cos_theta
        idx_ = torch.arange(0, nB, dtype=torch.long)
        output[idx_, label] = cos_theta_m[idx_, label]
        output *= self.s  # scale up in order to make softmax work, first introduced in normface
        return output


class LabelSmoothingCrossEntropy(nn.Module):
    def __init__(self, eps:float=0.1, reduction='mean'):
        super(LabelSmoothingCrossEntropy, self).__init__()

        self.eps,self.reduction = eps,reduction

    def forward(self, output, target):
        if type(output) is tuple:
            c = output[0].size()[-1]
        else:
            c = output.size()[-1]
        log_preds = F.log_softmax(output, dim=-1)
        if self.reduction=='sum': loss = -log_preds.sum()
        else:
            loss = -log_preds.sum(dim=-1)
            if self.reduction=='mean':  loss = loss.mean()
        return loss*self.eps/c + (1-self.eps) * F.nll_loss(log_preds, target, reduction=self.reduction)

class CustomArcface(nn.Module):
    def __init__(self, label_smoothing=True, use_arcface_loss=True,accloss = False):
        super(CustomArcface, self).__init__()
        # self.backbone = Backbone(conf.net_depth, conf.drop_ratio, conf.net_mode)
        self.arcface = use_arcface_loss
        self.head = ArcfaceFastAI()
        self.reduction = 'mean'
        self.accloss = accloss
        #self.start_arcface_from_epoch= args.start_arcface_from_epoch
        if label_smoothing:
            self.ce_loss = LabelSmoothingCrossEntropy(reduction=self.reduction, eps=0.03)
        else:
            self.ce_loss = nn.CrossEntropyLoss(reduction=self.reduction)
        self.net_output = None

    def forward(self, cos_theta, label,epoch=0,i=0):
        # features = self.backbone(x)
        #if not self.arcface and epoch >= self.start_arcface_from_epoch:
        #    self.arcface = True
        if not self.arcface:

            loss = self.ce_loss(cos_theta, label)
        else:
            self.net_output = self.head(cos_theta, label=label)
            loss = self.ce_loss(self.net_output, label)#.cpu()
        if self.accloss and i != -1:
            accloss = acc_func_deter(cos_theta, label)
            alpha, beta = getfactors(epoch)
            loss = alpha * loss + beta * accloss
            if i == 0:
                print("##########################################")
                print(" alpha:", alpha, "beta:", beta)
                print("##########################################")
        return loss


class F1_Loss(nn.Module):

    def __init__(self, epsilon=1e-7):
        super().__init__()
        self.epsilon = epsilon

    def forward(self, y_pred, y_true, ):
        assert y_pred.ndim == 2
        assert y_true.ndim == 1
        y_true = F.one_hot(y_true, 2).to(torch.float32)
        y_pred = F.softmax(y_pred, dim=1)

        tp = (y_true * y_pred).sum(dim=0).to(torch.float32)
        tn = ((1 - y_true) * (1 - y_pred)).sum(dim=0).to(torch.float32)
        fp = ((1 - y_true) * y_pred).sum(dim=0).to(torch.float32)
        fn = (y_true * (1 - y_pred)).sum(dim=0).to(torch.float32)

        precision = tp / (tp + fp + self.epsilon)
        recall = tp / (tp + fn + self.epsilon)

        f1 = 2 * (precision * recall) / (precision + recall + self.epsilon)
        f1 = f1.clamp(min=self.epsilon, max=1 - self.epsilon)
        return 1 - f1.mean()


f1_loss = F1_Loss().cuda()