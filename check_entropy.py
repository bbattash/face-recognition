
import argparse
import time
import os
import torch
import torch.distributed as dist
import torch.nn.functional as F
import torch.utils.data.distributed
from torch import nn
#from torch.utils.tensorboard import SummaryWriter
from torch import optim
from config import get_args
from ranger import Ranger
from loss_objectives import CustomArcface
from model import build_model_entropy
from models import mxresnet
from dataloaders import get_train_loader,get_val_data

from train import train
from validation import validate
from utils import save_checkpoint
#from torch.utils.tensorboard import SummaryWriter
from time import time
from dataloaders import get_custom_data
#from trains import Task
from entropy_utils.entropy_validation import validate
from entropy_utils.entropy_dl import get_val_loader,get_val_dataset

torch.backends.cudnn.benchmark = True
best_acc1 = 0

def main():
    global best_acc1
    args = get_args()


    val_loader,num_of_classes = get_val_loader(args)
    val_data = get_val_data(args)
    c_val_data=get_custom_data(args.custom_file,args.custom_path)
    #agedb_30, cfp_fp, lfw, ytf_rgb, agedb_30_issame, cfp_fp_issame, lfw_issame, imgs_df
    args.num_of_classes = num_of_classes

    model = build_model_entropy(args)
    if args.dp:
        model = torch.nn.DataParallel(model)
    model = model.cuda()

    criterion = CustomArcface(label_smoothing=args.label_smoothing, use_arcface_loss= args.use_arcface_loss,accloss=args.accloss).cuda()
    if args.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)
    elif args.optimizer == 'adam':
        optimizer = optim.Adam([var1, var2], lr=args.lr)
    else:
        optimizer = Ranger(model.parameters(),lr=args.lr, weight_decay=args.weight_decay)
    if args.freeze_bb:
        ft_optimizer = Ranger(model.parameters(),lr=args.lr_ft, weight_decay=args.weight_decay)

    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            if args.gpu is None:
                checkpoint = torch.load(args.resume)
            else:
                # Map model to be loaded to specified single gpu.
                loc = 'cuda:{}'.format(args.gpu)
                checkpoint = torch.load(args.resume, map_location=loc)
            args.start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']
            if args.gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(args.gpu)
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))
    #add hp for stages


    if args.load_path is "":
        print("You did not Load any CKPT!!!")
    else:
        print("Loading: ",args.load_path)
        dict= torch.load(args.load_path)['state_dict']
        if args.freeze_bb:
            del dict['module.kernel']
        model.load_state_dict(dict,strict=False)
    acc1, v_loss, ytf_acc, lfw_acc = validate(args, val_loader,  model)
if __name__ == '__main__':
    main()



